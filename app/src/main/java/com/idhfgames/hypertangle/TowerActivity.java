package com.idhfgames.hypertangle;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.idhfgames.hypertangle.core.AdTicker;
import com.idhfgames.hypertangle.core.Assets;
import com.idhfgames.hypertangle.core.Difficulty;
import com.idhfgames.hypertangle.core.Graph;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class TowerActivity extends Activity {
    public static Difficulty difficulty;
    private LinearLayout llTowerLevels;
    private ScrollView scrollViewTowerLevels;
    private int graphCount = 10;
    public static Graph[] graphs;
    private static long timeLeft;
    private static int finishedLevels;
    public static Button buttonOkFail;
    private StartAppAd startAppAd = new StartAppAd(this);
    public static boolean isActive = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            ActionBar actionBar = getActionBar();
            actionBar.hide();
        }
        Chartboost.startWithAppId(this, getString(R.string.chartboost_appid), getString(R.string.chartboost_signature));
        Chartboost.setDelegate(chartboostDelegate);
        Chartboost.onCreate(this);
        Chartboost.cacheInterstitial(CBLocation.LOCATION_GAME_SCREEN);
        StartAppSDK.init(this, getString(R.string.startapp_id), true);
        setContentView(R.layout.activity_tower);
        llTowerLevels = (LinearLayout) findViewById(R.id.llTowerLevels);
        scrollViewTowerLevels = (ScrollView) findViewById(R.id.scrollViewTowerLevels);
        if (graphs == null) {
            graphs = new Graph[graphCount];
            switch (difficulty) {
                case EASY: {
                    for (int i = 0; i < graphs.length; i++) {
                        graphs[i] = Graph.getRandomGraph(3, 6 + i / 5, Graph.TYPE.NONSCATTERED);
                    }
                    timeLeft = Assets.easyTowerTime;
                } break;
                case MEDIUM: {
                    for (int i = 0; i < graphs.length; i++) {
                        graphs[i] = Graph.getRandomGraph(3, 9 + i / 5, Graph.TYPE.NONSCATTERED);
                    }
                    timeLeft = Assets.mediumTowerTime;
                } break;
                case HARD: {
                    for (int i = 0; i < graphs.length; i++) {
                        graphs[i] = Graph.getRandomGraph(3, 10 + i / 5, Graph.TYPE.NONSCATTERED);
                    }
                    timeLeft = Assets.hardTowerTime;
                } break;
                case INFERNO: {
                    for (int i = 0; i < graphs.length; i++) {
                        graphs[i] = Graph.getRandomGraph(5, 15 + i / 5, Graph.TYPE.NONSCATTERED);
                    }
                    timeLeft = Assets.infernoTowerTime;
                } break;
            }

            for (int i = 0; i < graphs.length; i++) {
                for (int j = 0; j < i; j++) {
                    if (graphs[j].getDifficulty() < graphs[i].getDifficulty()) {
                        Graph graph = graphs[i];
                        graphs[i] = graphs[j];
                        graphs[j] = graph;
                    }
                }
            }
            finishedLevels = 0;
            Handler h = new Handler();
            h.postDelayed(new Runnable() {

                @Override
                public void run() {
                    scrollViewTowerLevels.smoothScrollTo(0, 2000);
                }
            }, 1000);
        }

        for (int i = 0; i < graphs.length; i++) {
            llTowerLevels.addView(getGraphView(i));
        }
        startAppAd.showAd();
        startAppAd.loadAd();
    }
    public void onButtonTowerBackClick(View view) {
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void onButtonTowerPlayClick(View view) {
        boolean b = Chartboost.hasInterstitial(CBLocation.LOCATION_GAME_SCREEN);
        if (!b) {
            Chartboost.cacheInterstitial(CBLocation.LOCATION_GAME_SCREEN);
        }
        if (AdTicker.isTime() && b) {
            Chartboost.showInterstitial(CBLocation.LOCATION_GAME_SCREEN);
        } else {
            GameActivity.setCurLevelId(-1);
            GameActivity.setGraph(graphs[graphs.length - finishedLevels - 1]);
            finish();
            Intent intent = new Intent(this, GameActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }
    public View getGraphView(int index) {
        LinearLayout llMain = null;// = new LinearLayout(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(3, 3, 3, 3);
        if (index < graphCount - finishedLevels - 1)
            llMain = (LinearLayout) getLayoutInflater().inflate(R.layout.template_tower_level_grey, null);
        else if (index == graphCount - finishedLevels - 1) {
            switch (difficulty) {
                case EASY: {
                    llMain = (LinearLayout) getLayoutInflater().inflate(R.layout.template_tower_level_green, null);
                } break;
                case MEDIUM: {
                    llMain = (LinearLayout) getLayoutInflater().inflate(R.layout.template_tower_level_orange, null);
                } break;
                case HARD: {
                    llMain = (LinearLayout) getLayoutInflater().inflate(R.layout.template_tower_level_red, null);
                } break;
                case INFERNO: {
                    llMain = (LinearLayout) getLayoutInflater().inflate(R.layout.template_tower_level_violet, null);
                } break;
            }
        } else {
            switch (difficulty) {
                case EASY: {
                    llMain = (LinearLayout) getLayoutInflater().inflate(R.layout.template_tower_level_green_dark, null);
                } break;
                case MEDIUM: {
                    llMain = (LinearLayout) getLayoutInflater().inflate(R.layout.template_tower_level_orange_dark, null);
                } break;
                case HARD: {
                    llMain = (LinearLayout) getLayoutInflater().inflate(R.layout.template_tower_level_red_dark, null);
                } break;
                case INFERNO: {
                    llMain = (LinearLayout) getLayoutInflater().inflate(R.layout.template_tower_level_violet_dark, null);
                } break;
            }
        }
        llMain.setGravity(Gravity.CENTER);
        llMain.setOrientation(LinearLayout.HORIZONTAL);
        llMain.setPadding(5, 5, 5, 5);
        llMain.setLayoutParams(params);

        LinearLayout llAdd2 = new LinearLayout(this);
        llAdd2.setGravity(Gravity.CENTER);

        TextView tVNumber = new TextView(this);
        tVNumber.setTypeface(MainMenuActivity.mainTypeface);
        tVNumber.setTextColor(getResources().getColor(R.color.flat_white));
        tVNumber.setTextSize(55);
        tVNumber.setLayoutParams(params);
        llAdd2.addView(tVNumber);
        llMain.addView(llAdd2);
        tVNumber.setText("" + (index + 1));
        tVNumber.setMinWidth(100);
        tVNumber.setGravity(Gravity.CENTER);

        LinearLayout llAdd = new LinearLayout(this);
        llAdd.setGravity(Gravity.CENTER);
        llAdd.setOrientation(LinearLayout.VERTICAL);
        llAdd.setPadding(5, 5, 5, 5);


        TextView tVNodes = new TextView(this);
        tVNodes.setTypeface(MainMenuActivity.mainTypeface);
        tVNodes.setTextColor(getResources().getColor(R.color.flat_white));
        tVNodes.setTextSize(21);
        llAdd.addView(tVNodes);
        tVNodes.setText("Nodes : " + graphs[index].getNodeNum());

        TextView tVLinks = new TextView(this);
        tVLinks.setTypeface(MainMenuActivity.mainTypeface);
        tVLinks.setTextColor(getResources().getColor(R.color.flat_white));
        tVLinks.setTextSize(21);
        llAdd.addView(tVLinks);
        tVLinks.setText("Links : " + graphs[index].getLinkNum());

        llMain.addView(llAdd);

        TextView tVDiff = new TextView(this);
        tVDiff.setTypeface(MainMenuActivity.mainTypeface);
        tVDiff.setTextColor(getResources().getColor(R.color.flat_white));
        tVDiff.setTextSize(34);
        llAdd.addView(tVDiff);
        tVDiff.setText("Difficulty : " + graphs[index].getDifficulty());
        return llMain;
    }
    public static void progress() {
        TowerActivity.finishedLevels++;
    }
    public static Long getTimeLeft() {
        return timeLeft;
    }
    public static void setTimeLeft(long timeLeft) {
        TowerActivity.timeLeft = timeLeft;
    }
    public static void reset() {
        graphs = null;
        finishedLevels = 0;
    }
    @Override
    protected void onStart() {
        super.onStart();
        Chartboost.onStart(this);
    }
    @Override
    public void onBackPressed() {
        if (Chartboost.onBackPressed())
            return;
        else
            super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    public static boolean isFinish() {
        return finishedLevels + 1 == graphs.length;
    }
    @Override
    public void onResume() {
        super.onResume();
        Chartboost.onResume(this);
        startAppAd.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        Chartboost.onPause(this);
        startAppAd.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        Chartboost.onStop(this);
        isActive = false;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Chartboost.onDestroy(this);
    }
    private ChartboostDelegate chartboostDelegate = new ChartboostDelegate() {
        @Override
        public boolean shouldDisplayInterstitial(String location) {
            return super.shouldDisplayInterstitial(location);
        }
    };
}

package com.idhfgames.hypertangle;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.games.Games;
import com.idhfgames.hypertangle.core.Achievment;
import com.idhfgames.hypertangle.core.AdTicker;
import com.idhfgames.hypertangle.core.Assets;
import com.idhfgames.hypertangle.core.MessagePopup;
import com.idhfgames.hypertangle.core.Options;
import com.idhfgames.hypertangle.core.Statistics;
import com.idhfgames.hypertangle.util.GamemodeActivity;
import com.idhfgames.hypertangle.util.OptionsActivity;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class MainMenuActivity extends Activity implements
        View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    public static boolean isActive = false;
    private static int RC_SIGN_IN = 9001;
    public static Typeface mainTypeface;
    public static Typeface addTypeface;
    public static Display display;
    public static MediaPlayer mpClick;
    Button buttonStart;
    Button buttonOptions;
    Button buttonStats;
    Button buttonExit;
    FrameLayout mainMenuFrame;
    private static Context context;
    public static DisplayMetrics dm = new DisplayMetrics();
    public static GoogleApiClient googleApiClient;
    private boolean mResolvingConnectionFailure = false;
    private boolean mAutoStartSignInflow = true;
    private boolean mSignInClicked = false;
    private boolean mExplicitSignOut = false;
    private boolean mInSignInFlow = false;
    private final int RESOLVE_CONNECTION_REQUEST_CODE = 1500;
    private AdTicker adTicker = new AdTicker();
    private StartAppAd startAppAd = new StartAppAd(this);
    private ArrayList<Achievment> achievments = new ArrayList<>();
    private MessagePopup bonusPopup;

    public static Context getContext() {
        return context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            ActionBar actionBar = getActionBar();
            actionBar.hide();
        }
        Chartboost.startWithAppId(this, getString(R.string.chartboost_appid), getString(R.string.chartboost_signature));
        Chartboost.setDelegate(chartboostDelegate);
        Chartboost.onCreate(this);
        Chartboost.cacheInterstitial(CBLocation.LOCATION_MAIN_MENU);
        Chartboost.cacheRewardedVideo(CBLocation.LOCATION_LEVEL_COMPLETE);
        StartAppSDK.init(this, getString(R.string.startapp_id), true);
        display = getWindowManager().getDefaultDisplay();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        setContentView(R.layout.activity_main_menu);
        getIntent().addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        googleApiClient  = new GoogleApiClient.Builder(this)
                .addApi(Games.API)
                .addScope(Drive.SCOPE_FILE)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        MainMenuActivity.context = this;
        mainTypeface = Typeface.createFromAsset(getAssets(), "SourceSansPro-Semibold.otf");
        addTypeface = Typeface.createFromAsset(getAssets(), "SourceSansPro-Light.otf");

        MainMenuActivity.mpClick = MediaPlayer.create(MainMenuActivity.context, R.raw.click);

        buttonStart = (Button) findViewById(R.id.buttonStart);
        buttonOptions = (Button) findViewById(R.id.buttonOptions);
        buttonStats = (Button) findViewById(R.id.buttonStats);
        buttonExit = (Button) findViewById(R.id.buttonExit);
        buttonStart.setTypeface(mainTypeface);
        buttonOptions.setTypeface(mainTypeface);
        buttonStats.setTypeface(mainTypeface);
        buttonExit.setTypeface(mainTypeface);

        mainMenuFrame = (FrameLayout) findViewById(R.id.mainMenuFrame);

        Options.loadOptions(getApplicationContext(), getSharedPreferences("stats", MODE_PRIVATE));

        Statistics.init(getApplicationContext(), getSharedPreferences("stats", MODE_PRIVATE));

        Achievment temp = new Achievment(5, 0, getString(R.string.achievement_get_over_here));
        Statistics.easyLevelsCompleted.addObserver(temp);
        achievments.add(temp);

        temp = new Achievment(Assets.easyLevelNum, 0, getString(R.string.achievement_that_was_easy));
        Statistics.easyLevelsCompleted.addObserver(temp);
        achievments.add(temp);

        temp = new Achievment(5, 0, getString(R.string.achievement_tower_2_0));
        Statistics.mediumLevelsCompleted.addObserver(temp);
        achievments.add(temp);

        temp = new Achievment(Assets.mediumLevelNum, 0, getString(R.string.achievement_still_pretty_easy));
        Statistics.mediumLevelsCompleted.addObserver(temp);
        achievments.add(temp);

        temp = new Achievment(5, 0, getString(R.string.achievement_harder_then_it_looks));
        Statistics.hardLevelsCompleted.addObserver(temp);
        achievments.add(temp);

        temp = new Achievment(Assets.hardLevelNum, 0, getString(R.string.achievement_getting_harder));
        Statistics.hardLevelsCompleted.addObserver(temp);
        achievments.add(temp);

        temp = new Achievment(1, 0, getString(R.string.achievement_on_a_way_to_hell));
        Statistics.infernoLevelsCompleted.addObserver(temp);
        achievments.add(temp);

        temp = new Achievment(5, 0, getString(R.string.achievement_impossibru));
        Statistics.infernoLevelsCompleted.addObserver(temp);
        achievments.add(temp);

        temp = new Achievment(Assets.infernoLevelNum, 0, getString(R.string.achievement_god));
        Statistics.infernoLevelsCompleted.addObserver(temp);
        achievments.add(temp);

        temp = new Achievment(1, 0, getString(R.string.achievement_easy_tower));
        Statistics.easyTowersCompleted.addObserver(temp);
        achievments.add(temp);

        temp = new Achievment(1, 0, getString(R.string.achievement_medium_tower));
        Statistics.mediumTowersCompleted.addObserver(temp);
        achievments.add(temp);

        temp = new Achievment(1, 0, getString(R.string.achievement_hard_tower));
        Statistics.hardTowersCompleted.addObserver(temp);
        achievments.add(temp);

        temp = new Achievment(1, 0, getString(R.string.achievement_inferno_tower));
        Statistics.infernoTowersCompleted.addObserver(temp);
        achievments.add(temp);

        temp = new Achievment(1000 * 3600, 0, getString(R.string.achievement_1_hour));
        Statistics.totalTime.addObserver(temp);
        achievments.add(temp);

        temp = new Achievment(10000 * 3600, 0, getString(R.string.achievement_10_hours));
        Statistics.totalTime.addObserver(temp);
        achievments.add(temp);

        temp = new Achievment(100000 * 3600, 0, getString(R.string.achievement_100_hours));
        Statistics.totalTime.addObserver(temp);
        achievments.add(temp);

        Statistics.loadStatistics();

        mainMenuFrame.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        Calendar calendar = Calendar.getInstance();
                        if (calendar.get(Calendar.DAY_OF_MONTH) != Statistics.lastSkipGatherDay.getData()) {

                            if (Statistics.lastSkipGatherDay.getData() != 0) {
                                Statistics.skipCount.setData(Statistics.skipCount.getData() + 1);
                                Statistics.lastSkipGatherDay.setData(calendar.get(Calendar.DAY_OF_MONTH));
                                bonusPopup = new MessagePopup(context, getResources().getString(R.string.daily_bonus), getResources().getString(R.string._1_skip), "OK", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        bonusPopup.getPopup().dismiss();
                                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                    }
                                });
                            } else {
                                Statistics.skipCount.setData(5);
                                Statistics.lastSkipGatherDay.setData(calendar.get(Calendar.DAY_OF_MONTH));
                                bonusPopup = new MessagePopup(context, getResources().getString(R.string.new_incomer), getResources().getString(R.string._5_skip), "OK", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        bonusPopup.getPopup().dismiss();
                                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                    }
                                });
                            }
                            bonusPopup.getPopup().showAtLocation(mainMenuFrame, Gravity.CENTER, 0, 0);
                            bonusPopup.getPopup().setWindowLayoutMode(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                            bonusPopup.getPopup().update(0, 0, 300, 80);
                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        }
                        mainMenuFrame.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                });

        findViewById(R.id.sign_in_button).setOnClickListener(this);
        findViewById(R.id.sign_out_button).setOnClickListener(this);
        ((Button)findViewById(R.id.sign_out_button)).setTypeface(mainTypeface);

        startAppAd.showAd();
        startAppAd.loadAd();

    }
    public void onStartButtonClick(View view) {
        mpClick.start();
        boolean b = Chartboost.hasInterstitial(CBLocation.LOCATION_MAIN_MENU);
        if (!b) {
            Chartboost.cacheInterstitial(CBLocation.LOCATION_MAIN_MENU);
        }
        if (AdTicker.isTime() && b) {
            Chartboost.showInterstitial(CBLocation.LOCATION_MAIN_MENU);
        } else {
            Intent intent = new Intent(this, GamemodeActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }
    public void onOptionsButtonClick(View view) {
        mpClick.start();
        boolean b = Chartboost.hasInterstitial(CBLocation.LOCATION_MAIN_MENU);
        if (!b) {
            Chartboost.cacheInterstitial(CBLocation.LOCATION_MAIN_MENU);
        }
        if (AdTicker.isTime() && b) {
            Chartboost.showInterstitial(CBLocation.LOCATION_MAIN_MENU);
        } else {
            Intent intent = new Intent(this, OptionsActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }
    public void onStatButtonClick(View view) {
        mpClick.start();
        boolean b = Chartboost.hasInterstitial(CBLocation.LOCATION_MAIN_MENU);
        if (!b) {
            Chartboost.cacheInterstitial(CBLocation.LOCATION_MAIN_MENU);
        }
        if (AdTicker.isTime() && b) {
            Chartboost.showInterstitial(CBLocation.LOCATION_MAIN_MENU);
        } else {
            Intent intent = new Intent(this, StatsActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }
    public void onExitButtonClick(View view) {
        mpClick.start();
        this.finish();
    }
    public void onAchievmentsButtonClick(View view) {
        boolean b = Chartboost.hasInterstitial(CBLocation.LOCATION_MAIN_MENU);
        if (!b) {
            Chartboost.cacheInterstitial(CBLocation.LOCATION_MAIN_MENU);
        }
        if (AdTicker.isTime() && b) {
            Chartboost.showInterstitial(CBLocation.LOCATION_MAIN_MENU);
        } else {
            if (googleApiClient.isConnected()) {
                startActivityForResult(Games.Achievements.getAchievementsIntent(MainMenuActivity.googleApiClient), 2);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else {
                googleApiClient.connect();
            }
        }
    }
    public void onLeaderboardButtonClick(View view) {
        boolean b = Chartboost.hasInterstitial(CBLocation.LOCATION_MAIN_MENU);
        if (!b) {
            Chartboost.cacheInterstitial(CBLocation.LOCATION_MAIN_MENU);
        }
        if (AdTicker.isTime() && b) {
            Chartboost.showInterstitial(CBLocation.LOCATION_MAIN_MENU);
        } else {
            if (googleApiClient.isConnected()) {
                startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(googleApiClient), 3);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else {
                googleApiClient.connect();
            }
        }
    }
    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
    }

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    /**
     * Method to verify google play services on the device
     * */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }
    @Override
    public void onConnected(Bundle bundle) {
        findViewById(R.id.sign_in_button).setVisibility(View.GONE);
        findViewById(R.id.sign_out_button).setVisibility(View.VISIBLE);
        Statistics.notifyAchievements();
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.sign_in_button) {


            mSignInClicked = true;
            googleApiClient.connect();
        }
        else if (view.getId() == R.id.sign_out_button) {
            mExplicitSignOut = true;
            if (googleApiClient != null && googleApiClient.isConnected()) {
                Games.signOut(googleApiClient);
                googleApiClient.disconnect();
            }

            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
            findViewById(R.id.sign_out_button).setVisibility(View.GONE);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, RESOLVE_CONNECTION_REQUEST_CODE);
            } catch (IntentSender.SendIntentException e) {
            }
        } else {
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this, 0).show();
        }
    }
    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            case RESOLVE_CONNECTION_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    googleApiClient.connect();
                }
                break;
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        isActive = true;
        Chartboost.onStart(this);
    }
    @Override
    public void onResume() {
        super.onResume();
        Chartboost.onResume(this);
        startAppAd.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        Chartboost.onPause(this);
        startAppAd.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        Chartboost.onStop(this);
    }
    @Override
    public void onDestroy() {
        Statistics.saveStatistics();
        Options.saveOptions();
        super.onDestroy();
        Chartboost.onDestroy(this);
    }
    @Override
    public void onBackPressed() {
        if (Chartboost.onBackPressed())
            return;
        else
            super.onBackPressed();
    }

    private ChartboostDelegate chartboostDelegate = new ChartboostDelegate() {
        @Override
        public boolean shouldDisplayInterstitial(String location) {
            return super.shouldDisplayInterstitial(location);
        }
    };

    private void recalculateLevelProgress() {
        int counter = 0;
        for (int i = 0; i < Assets.easyLevelNum; i++) {
            if (Statistics.levelTimeHighScores.getData(i + 1) != -1) {
                counter++;
            }
        }
        Statistics.easyLevelsCompleted.setData(counter);
        counter = 0;
        for (int i = 0; i < Assets.mediumLevelNum; i++) {
            if (Statistics.levelTimeHighScores.getData(i + 1 + Assets.easyLevelNum) != -1) {
                counter++;
            }
        }
        Statistics.mediumLevelsCompleted.setData(counter);
        counter = 0;
        for (int i = 0; i < Assets.hardLevelNum; i++) {
            if (Statistics.levelTimeHighScores.getData(i + 1 + Assets.easyLevelNum + Assets.mediumLevelNum) != -1) {
                counter++;
            }
        }
        Statistics.hardLevelsCompleted.setData(counter);
        counter = 0;
        for (int i = 0; i < Assets.infernoLevelNum; i++) {
            if (Statistics.levelTimeHighScores.getData(i + 1 + Assets.easyLevelNum + Assets.mediumLevelNum + Assets.hardLevelNum) != -1) {
                counter++;
            }
        }
        Statistics.infernoLevelsCompleted.setData(counter);}
}

package com.idhfgames.hypertangle;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.idhfgames.hypertangle.core.Difficulty;
import com.idhfgames.hypertangle.core.Statistics;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class TowerDifficultyActivity extends Activity {
    Button buttonEasy;
    Button buttonMedium;
    Button buttonHard;
    Button buttonInferno;
    private StartAppAd startAppAd = new StartAppAd(this);
    public static boolean isActive = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            ActionBar actionBar = getActionBar();
            actionBar.hide();
        }
        StartAppSDK.init(this, getString(R.string.startapp_id), true);
        setContentView(R.layout.activity_tower_difficulty);

        buttonEasy = (Button) findViewById(R.id.buttonEasy);
        buttonMedium = (Button) findViewById(R.id.buttonMedium);
        buttonHard = (Button) findViewById(R.id.buttonHard);
        buttonInferno = (Button) findViewById(R.id.buttonInferno);

        if (Statistics.easyLevelsCompleted.getData() < 5) {
            buttonEasy.setBackgroundColor(getResources().getColor(R.color.flat_grey));
        } else {
            buttonEasy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEasyClick(v);
                }
            });
        }

        if (Statistics.mediumLevelsCompleted.getData() < 5) {
            buttonMedium.setBackgroundColor(getResources().getColor(R.color.flat_grey));
        } else {
            buttonMedium.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onMediumClick(v);
                }
            });
        }
        if (Statistics.hardLevelsCompleted.getData() < 5) {
            buttonHard.setBackgroundColor(getResources().getColor(R.color.flat_grey));
        } else {
            buttonHard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onHardClick(v);
                }
            });
        }
        if (Statistics.infernoLevelsCompleted.getData() < 5) {
            buttonInferno.setBackgroundColor(getResources().getColor(R.color.flat_grey));
        } else {
            buttonInferno.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onInfernoClick(v);
                }
            });
        }
        startAppAd.showAd();
        startAppAd.loadAd();
    }
    public void onEasyClick(View view) {
        MainMenuActivity.mpClick.start();
        TowerActivity.difficulty = Difficulty.EASY;
        TowerActivity.graphs = null;
        Intent intent = new Intent(this, TowerActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    public void onMediumClick(View view) {
        MainMenuActivity.mpClick.start();
        TowerActivity.difficulty = Difficulty.MEDIUM;
        TowerActivity.graphs = null;
        Intent intent = new Intent(this, TowerActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    public void onHardClick(View view) {
        MainMenuActivity.mpClick.start();
        TowerActivity.difficulty = Difficulty.HARD;
        TowerActivity.graphs = null;
        Intent intent = new Intent(this, TowerActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    public void onInfernoClick(View view) {
        MainMenuActivity.mpClick.start();
        TowerActivity.difficulty = Difficulty.INFERNO;
        TowerActivity.graphs = null;
        Intent intent = new Intent(this, TowerActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    @Override
    public void onBackPressed() {
        this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    @Override
    public void onResume() {
        super.onResume();
        startAppAd.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        startAppAd.onPause();
    }
    @Override
    public void onStart() {
        super.onStart();
        isActive = true;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        isActive = false;
    }
}

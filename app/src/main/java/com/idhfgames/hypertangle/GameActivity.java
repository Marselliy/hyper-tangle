package com.idhfgames.hypertangle;


import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.util.Pair;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.chartboost.sdk.Model.CBError;
import com.idhfgames.hypertangle.core.AdTicker;
import com.idhfgames.hypertangle.core.Assets;
import com.idhfgames.hypertangle.core.CanvasFrame;
import com.idhfgames.hypertangle.core.Graph;
import com.idhfgames.hypertangle.core.Options;
import com.idhfgames.hypertangle.core.Statistics;
import com.idhfgames.hypertangle.util.GamemodeActivity;
import com.idhfgames.hypertangle.util.LevelSelectActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;



public class GameActivity extends Activity {

    private FrameLayout dimBackground;
    private static Graph graph;
    private static int curLevelId;
    private FrameLayout gameFrame;
    private CanvasFrame canvasFrame;
    private Chronometer levelTimer;
    private RelativeLayout gameButtons;
    private int moveCount;
    private PopupWindow levelFinishWindow;
    private PopupWindow levelSkipWindow;
    private TextView textViewTime;
    private TextView textViewMoves;
    private TextView textViewTimeRecord;
    private TextView textViewMovesRecord;
    private Point size;
    private PopupWindow popupTowerFinish;
    final Context context = this;
    float m_lastTouchX = -1, m_lastTouchY = -1, m_posX, m_posY, m_prevX, m_prevY, m_dx, m_dy;
    private CountDownTimer towerTimer;
    private static boolean adInitialized;

    public static void setCurLevelId(int curLevelId) {
        GameActivity.curLevelId = curLevelId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            ActionBar actionBar = getActionBar();
            actionBar.hide();
        }
        if (!GameActivity.adInitialized) {
            Chartboost.startWithAppId(this, getString(R.string.chartboost_appid), getString(R.string.chartboost_signature));
            Chartboost.setDelegate(chartboostDelegate);
            Chartboost.onCreate(this);
            Chartboost.cacheRewardedVideo(CBLocation.LOCATION_LEVEL_COMPLETE);
            Chartboost.cacheInterstitial(CBLocation.LOCATION_LEVEL_COMPLETE);
            GameActivity.adInitialized = true;
        }
        setContentView(R.layout.activity_game);
        dimBackground = (FrameLayout) findViewById(R.id.game_dimBackground);
        gameButtons = (RelativeLayout) findViewById(R.id.gameButtons);
        levelTimer = (Chronometer) findViewById(R.id.levelTimer);
        if (curLevelId != -1)
            levelTimer.start();
        gameFrame = (FrameLayout) findViewById(R.id.gameFrame);
        canvasFrame = new CanvasFrame(this, graph);
        gameFrame.addView(canvasFrame);
        moveCount = 0;
        Display display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        canvasFrame.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        levelFinishWindow = new PopupWindow(this);
        LinearLayout llMain = new LinearLayout(this);
        llMain.setBackgroundDrawable(getResources().getDrawable(R.drawable.popup_level_finish));
        llMain.setGravity(Gravity.CENTER);
        llMain.setPadding(5, 5, 5, 5);

        TextView tV = new TextView(this);
        tV.setTypeface(MainMenuActivity.addTypeface);
        tV.setTextColor(getResources().getColor(R.color.flat_white));
        tV.setTextSize(34);
        llMain.addView(tV);
        tV.setText("Level completed!");

        LinearLayout llAdd = new LinearLayout(this);
        llAdd.setOrientation(LinearLayout.VERTICAL);
        llAdd.setGravity(Gravity.LEFT);
        llAdd.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        llMain.addView(llAdd);

        textViewTime = new TextView(this);
        textViewTime.setTypeface(MainMenuActivity.addTypeface);
        textViewTime.setTextColor(getResources().getColor(R.color.flat_white));
        textViewTime.setTextSize(21);
        llAdd.addView(textViewTime);

        if (curLevelId != -1) {
            textViewTimeRecord = new TextView(this);
            textViewTimeRecord.setTypeface(MainMenuActivity.addTypeface);
            textViewTimeRecord.setTextColor(getResources().getColor(R.color.flat_orange));
            textViewTimeRecord.setTextSize(21);
            llAdd.addView(textViewTimeRecord);
        }
            textViewMoves = new TextView(this);
            textViewMoves.setTypeface(MainMenuActivity.addTypeface);
            textViewMoves.setTextColor(getResources().getColor(R.color.flat_white));
            textViewMoves.setTextSize(21);
            llAdd.addView(textViewMoves);


        if (curLevelId != -1) {
            textViewMovesRecord = new TextView(this);
            textViewMovesRecord.setTypeface(MainMenuActivity.addTypeface);
            textViewMovesRecord.setTextColor(getResources().getColor(R.color.flat_orange));
            textViewMovesRecord.setTextSize(21);
            llAdd.addView(textViewMovesRecord);
        }

        LinearLayout glAdd = new LinearLayout(this);
        glAdd.setBackgroundColor(Color.argb(0, 0, 0, 0));

        llMain.addView(glAdd);
        llMain.setOrientation(LinearLayout.VERTICAL);
        GridLayout.Spec row0 = GridLayout.spec(0);
        GridLayout.Spec col1 = GridLayout.spec(0);
        GridLayout.Spec col2 = GridLayout.spec(1);
        GridLayout.Spec col3 = GridLayout.spec(2);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(3, 3, 3, 3);

        Button buttonBack = new Button(this);
        buttonBack.setTypeface(MainMenuActivity.addTypeface);
        buttonBack = (Button) getLayoutInflater().inflate(R.layout.template_level_select_button_red, null);
        buttonBack.setPadding(8, 8, 8, 8);
        buttonBack.setTextSize(21);
        glAdd.addView(buttonBack);
        buttonBack.setLayoutParams(params);
        buttonBack.setText("Back");
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainMenuActivity.mpClick.start();
                dimBackground.setVisibility(View.GONE);
                if (curLevelId == 0)
                    Statistics.randomLevelsCompleted.setData(Statistics.randomLevelsCompleted.getData() + 1);
                if (curLevelId == -1) {
                    TowerActivity.progress();
                }
                onLevelSelectButtonClick(v);
            }
        });


        Button buttonRestart = new Button(this);
        buttonRestart.setTypeface(MainMenuActivity.addTypeface);
        if (curLevelId != -1)
            buttonRestart = (Button) getLayoutInflater().inflate(R.layout.template_level_select_button_orange, null);
        else
            buttonRestart = (Button) getLayoutInflater().inflate(R.layout.template_level_button_locked, null);
        buttonRestart.setPadding(8, 8, 8, 8);
        buttonRestart.setTextSize(21);
        glAdd.addView(buttonRestart);
        buttonRestart.setLayoutParams(params);
        buttonRestart.setText("Restart");
        if (curLevelId != -1) {
            MainMenuActivity.mpClick.start();
            buttonRestart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dimBackground.setVisibility(View.GONE);
                    onReloadButtonCLick(v);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    levelFinishWindow.dismiss();
                }
            });
        }

        Button buttonNextLevel = new Button(this);
        buttonNextLevel.setTypeface(MainMenuActivity.addTypeface);
        buttonNextLevel.setTextColor(getResources().getColor(R.color.main_menu_font_color));
        if (curLevelId != -1)
            buttonNextLevel = (Button) getLayoutInflater().inflate(R.layout.template_level_select_button_green, null);
        else
            buttonNextLevel = (Button) getLayoutInflater().inflate(R.layout.template_level_button_locked, null);
        buttonNextLevel.setTextSize(21);
        buttonNextLevel.setPadding(8, 8, 8, 8);
        glAdd.addView(buttonNextLevel);
        buttonNextLevel.setLayoutParams(params);
        buttonNextLevel.setText("Next level");
        if (curLevelId != -1 && curLevelId != Assets.getTotalLevelNum()) {
            buttonNextLevel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainMenuActivity.mpClick.start();
                    dimBackground.setVisibility(View.GONE);
                    saveLevelStats();
                    Graph graph;
                    if (curLevelId == 0) {
                        graph = Graph.getRandomGraph(4, LevelSelectActivity.randomNodeCount, Graph.TYPE.NONSCATTERED);
                    } else {
                        StringBuilder data = new StringBuilder("");
                        BufferedReader reader = null;
                        try {
                            reader = new BufferedReader(new InputStreamReader(getAssets().open("levels/" + (curLevelId + 1) + ".lvl")));


                            String mLine = reader.readLine();
                            while (mLine != null) {
                                data = data.append(mLine);

                                mLine = reader.readLine();
                            }
                        } catch (IOException e) {

                        } finally {
                            if (reader != null) {
                                try {
                                    reader.close();
                                } catch (IOException e) {

                                }
                            }
                        }
                        graph = new Graph(data.toString());
                    }
                    graph.isPlanar();
                    setGraph(graph);
                    curLevelId += (curLevelId > 0 ? 1 : 0);
                    if (curLevelId == 0)
                        Statistics.randomLevelsCompleted.setData(Statistics.randomLevelsCompleted.getData() + 1);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    levelFinishWindow.dismiss();
                    finish();
                    boolean b = Chartboost.hasRewardedVideo(CBLocation.LOCATION_LEVEL_COMPLETE);
                    if (!b) {
                        Chartboost.cacheRewardedVideo(CBLocation.LOCATION_LEVEL_COMPLETE);
                        if (Chartboost.hasInterstitial(CBLocation.LOCATION_LEVEL_COMPLETE)) {
                            Chartboost.showInterstitial(CBLocation.LOCATION_LEVEL_COMPLETE);
                        }
                    }
                    if (AdTicker.isTime() && b) {
                        Chartboost.showRewardedVideo(CBLocation.LOCATION_LEVEL_COMPLETE);
                    } else {
                        startActivity(getIntent());
                    }
                }
            });
        }


        levelFinishWindow.setContentView(llMain);


        if (curLevelId == -1) {
            towerTimer = new CountDownTimer(TowerActivity.getTimeLeft(), 1000) {

                public void onTick(long millisUntilFinished) {
                    levelTimer.setBase(-millisUntilFinished + SystemClock.elapsedRealtime());
                    TowerActivity.setTimeLeft(TowerActivity.getTimeLeft() - 1000);
                }

                public void onFinish() {
                    PopupWindow failPopup = new PopupWindow(context);
                    LinearLayout llMain = new LinearLayout(context);
                    llMain.setBackgroundDrawable(getResources().getDrawable(R.drawable.popup_level_finish));
                    llMain.setGravity(Gravity.CENTER);
                    llMain.setPadding(5, 5, 5, 5);
                    llMain.setOrientation(LinearLayout.VERTICAL);

                    TextView tV = new TextView(context);
                    tV.setTypeface(MainMenuActivity.addTypeface);
                    tV.setTextColor(getResources().getColor(R.color.flat_red));
                    tV.setTextSize(34);
                    tV.setText("Failed!");
                    llMain.addView(tV);

                    dimBackground.setVisibility(View.VISIBLE);
                    TextView tV2 = new TextView(context);
                    tV2.setTypeface(MainMenuActivity.addTypeface);
                    tV2.setTextColor(getResources().getColor(R.color.flat_white));
                    tV2.setTextSize(21);
                    tV2.setText("Time is over!");
                    llMain.addView(tV2);

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(3, 3, 3, 3);

                    Button buttonOkFail = new Button(context);
                    buttonOkFail.setTypeface(MainMenuActivity.addTypeface);
                    buttonOkFail = (Button) getLayoutInflater().inflate(R.layout.template_level_select_button_red, null);
                    buttonOkFail.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    buttonOkFail.setPadding(8, 8, 8, 8);
                    buttonOkFail.setTextSize(21);
                    llMain.addView(buttonOkFail);
                    buttonOkFail.setLayoutParams(params);
                    buttonOkFail.setText("OK");
                    buttonOkFail.setMinWidth(300);
                    towerTimer.cancel();
                    buttonOkFail.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onLevelFail();
                            TowerActivity.reset();
                            dimBackground.setVisibility(View.GONE);
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        }
                    });
                    failPopup.setContentView(llMain);
                    failPopup.setAnimationStyle(R.style.PopupAnimation);
                    failPopup.showAtLocation(gameFrame, Gravity.CENTER, 0, 0);
                    failPopup.setWindowLayoutMode(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    failPopup.update(0, 0, 300, 80);
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
            }.start();
        }

        for (int i = 0; i < graph.nodes.length; i++) {
            graph.nodes[i] = new Button(this);
            graph.nodes[i] = (Button)getLayoutInflater().inflate(R.layout.template_node_button, null);
            graph.nodes[i].setX(graph.getNodeCoords()[i].x - graph.getNodeSize() / 2);
            graph.nodes[i].setY(graph.getNodeCoords()[i].y - graph.getNodeSize() / 2);
            graph.nodes[i].setLayoutParams(new FrameLayout.LayoutParams(graph.getNodeSize(), graph.getNodeSize()));


            graph.nodes[i].setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View p_v, MotionEvent p_event) {

                    switch (p_event.getAction()) {
                        case MotionEvent.ACTION_DOWN: {
                            moveCount++;
                            m_lastTouchX = p_event.getX();
                            m_lastTouchY = p_event.getY();
                            break;
                        }
                        case MotionEvent.ACTION_UP: {
                            Pair<ArrayList<Integer>, ArrayList<Integer>> problems = graph.isRealized();
                            canvasFrame.problemLinks = problems.first;
                            canvasFrame.problemNodes = problems.second;
                            if (canvasFrame.problemLinks.size() == 0) {
                                onLevelFinish();
                            }
                            break;
                        }

                        case MotionEvent.ACTION_MOVE: {
                            m_dx = p_event.getX() - m_lastTouchX;
                            m_dy = p_event.getY() - m_lastTouchY;

                            m_posX = p_v.getX() + m_dx;
                            m_posY = p_v.getY() + m_dy;

                            if (true) {
                                p_v.setLayoutParams(new FrameLayout.LayoutParams(p_v.getMeasuredWidth(), p_v.getMeasuredHeight()));
                                p_v.setX(m_posX);
                                p_v.setY(m_posY);

                                m_prevX = m_posX;
                                m_prevY = m_posY;

                            }

                            if (Options.realTimeIntersections) {
                                Pair<ArrayList<Integer>, ArrayList<Integer>> problems = graph.isRealized();
                                canvasFrame.problemLinks = problems.first;
                                canvasFrame.problemNodes = problems.second;
                            }

                            break;
                        }
                    }
                    canvasFrame.invalidate();
                    return true;
                }
            });
            canvasFrame.addView(graph.nodes[i]);
        }
        Pair<ArrayList<Integer>, ArrayList<Integer>> problems = graph.isRealized();
        canvasFrame.problemLinks = problems.first;
        canvasFrame.problemNodes = problems.second;

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            graph.transpose();
            transposeSize();
        }
    }
    public void onLevelSelectButtonClick(View view) {
        MainMenuActivity.mpClick.start();
        Intent intent;
        if (curLevelId != -1) {
            intent = new Intent(this, LevelSelectActivity.class);
            TowerActivity.progress();
        } else {
            towerTimer.cancel();
            intent = new Intent(this, TowerActivity.class);
        }
        this.finish();
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    public static void setGraph(Graph graph) {
        GameActivity.graph = graph;
    }
    @Override
    public void onDestroy() {
        saveLevelStats();
        Chartboost.onDestroy(this);
        super.onDestroy();
    }
    public void onReloadButtonCLick(View view) {
        MainMenuActivity.mpClick.start();
        for (int i = 0; i < graph.nodes.length; i++) {
            graph.nodes[i].setX(graph.getNodeCoords()[i].x - graph.getNodeSize() / 2);
            graph.nodes[i].setY(graph.getNodeCoords()[i].y - graph.getNodeSize() / 2);
        }
        saveLevelStats();
        Pair<ArrayList<Integer>, ArrayList<Integer>> problems = graph.isRealized();
        canvasFrame.problemLinks = problems.first;
        canvasFrame.problemNodes = problems.second;
        canvasFrame.invalidate();
    }
    private void saveLevelStats() {
        if (curLevelId != -1) {
            Statistics.totalTime.setData(Statistics.totalTime.getData() + ((SystemClock.elapsedRealtime() - levelTimer.getBase())));
        } else {
            long time = SystemClock.elapsedRealtime() - levelTimer.getBase();
            switch (TowerActivity.difficulty) {
                case EASY: {
                    time -= Assets.easyTowerTime;
                } break;
                case MEDIUM: {
                    time -= Assets.mediumTowerTime;
                } break;
                case HARD: {
                    time -= Assets.hardTowerTime;
                } break;
                case INFERNO: {
                    time -= Assets.infernoTowerTime;
                } break;
            }
            Statistics.totalTime.setData(Statistics.totalTime.getData() - time);
        }
        Statistics.moveCount.setData(Statistics.moveCount.getData() + moveCount);
        if (curLevelId != -1) {
            levelTimer.setBase(SystemClock.elapsedRealtime());
            levelTimer.start();
        }
        moveCount = 0;
    }
    public void onSkipButtonClick(View view) {
        MainMenuActivity.mpClick.start();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(3, 3, 3, 3);
        levelSkipWindow = new PopupWindow(this);
        LinearLayout llMain2 = new LinearLayout(this);
        llMain2.setBackgroundDrawable(getResources().getDrawable(R.drawable.popup_level_finish));
        llMain2.setOrientation(LinearLayout.VERTICAL);
        llMain2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        llMain2.setGravity(Gravity.CENTER);
        llMain2.setPadding(5, 5, 5, 5);

        TextView tV2 = new TextView(this);
        tV2.setTypeface(MainMenuActivity.addTypeface);
        tV2.setTextColor(getResources().getColor(R.color.flat_white));
        tV2.setTextSize(34);
        llMain2.addView(tV2);
        tV2.setText("Skip level?");

        TextView textViewSkips = new TextView(this);
        textViewSkips.setTypeface(MainMenuActivity.addTypeface);
        textViewSkips.setTextColor(getResources().getColor(R.color.flat_white));
        textViewSkips.setTextSize(21);
        textViewSkips.setText("You have " + Statistics.skipCount.getData() + " skips");
        llMain2.addView(textViewSkips);

        LinearLayout llAdd2 = new LinearLayout(this);
        llAdd2.setOrientation(LinearLayout.HORIZONTAL);
        llAdd2.setGravity(Gravity.LEFT);
        llAdd2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        llMain2.addView(llAdd2);


        Button buttonNo = new Button(context);
        buttonNo.setTypeface(MainMenuActivity.addTypeface);
        buttonNo = (Button)getLayoutInflater().inflate(getResources().getLayout(R.layout.template_level_select_button_green), null);
        buttonNo.setLayoutParams(params);
        buttonNo.setMinWidth(150);


        buttonNo.setTextSize(26);
        buttonNo.setText("No");
        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                levelSkipWindow.dismiss();
                if (curLevelId != -1)
                    levelTimer.start();
            }
        });
        llAdd2.addView(buttonNo);

        Button buttonYes = new Button(context);
        buttonYes.setTypeface(MainMenuActivity.addTypeface);
        if (Statistics.skipCount.getData() > 0 && curLevelId > 0 && graph.hasSolution()) {
            buttonYes = (Button) getLayoutInflater().inflate(getResources().getLayout(R.layout.template_level_select_button_red), null);
            buttonYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainMenuActivity.mpClick.start();
                    graph.applySolution();
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    Pair<ArrayList<Integer>, ArrayList<Integer>> problems = graph.isRealized();
                    canvasFrame.problemLinks = problems.first;
                    canvasFrame.problemNodes = problems.second;
                    canvasFrame.invalidate();
                    levelSkipWindow.dismiss();
                    Statistics.skipCount.setData(Statistics.skipCount.getData() - 1);
                    moveCount = -1;
                    onLevelFinish();
                }
            });
        } else {
            buttonYes = (Button) getLayoutInflater().inflate(getResources().getLayout(R.layout.template_level_button_locked), null);
        }
        buttonYes.setLayoutParams(params);
        buttonYes.setMinWidth(150);



        buttonYes.setTextSize(26);
        buttonYes.setText("Yes");
        llAdd2.addView(buttonYes);

        levelSkipWindow.setContentView(llMain2);
        levelSkipWindow.showAtLocation(gameFrame, Gravity.CENTER, 0, 0);
        levelSkipWindow.setWindowLayoutMode(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        levelSkipWindow.update(0, 0, 300, 80);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        levelTimer.stop();
    }
    private void onLevelFinish() {
        dimBackground.setVisibility(View.VISIBLE);
        if (curLevelId != -1 || !TowerActivity.isFinish()) {
            int time = (int) ((SystemClock.elapsedRealtime() - levelTimer.getBase()));
            int hours = time / 3600000;
            int minutes = (time - hours * 3600000) / 60000;
            int seconds = (time - hours * 3600000 - minutes * 60000) / 1000;
            int ms = time - hours * 3600000 - minutes * 60000 - seconds * 1000;
            if (curLevelId != -1) {
                if (curLevelId > 0 && Statistics.levelTimeHighScores.getData(curLevelId) == -1) {
                    if (curLevelId <= Assets.easyLevelNum) {
                        Statistics.easyLevelsCompleted.setData(Statistics.easyLevelsCompleted.getData() + 1);
                    } else if (curLevelId <= Assets.easyLevelNum + Assets.mediumLevelNum) {
                        Statistics.mediumLevelsCompleted.setData(Statistics.mediumLevelsCompleted.getData() + 1);
                    } else if (curLevelId <= Assets.easyLevelNum + Assets.mediumLevelNum + Assets.hardLevelNum) {
                        Statistics.hardLevelsCompleted.setData(Statistics.hardLevelsCompleted.getData() + 1);
                    } else {
                        Statistics.infernoLevelsCompleted.setData(Statistics.infernoLevelsCompleted.getData() + 1);
                    }
                }
                if (moveCount != -1) {
                    textViewTime.setText("Time : " + (hours == 0 ? "" : (hours + "h ")) + (minutes == 0 ? "" : (minutes + "m ")) + seconds + "." + ms + "s");
                    if (Statistics.levelTimeHighScores.getData(curLevelId) == -1 || Statistics.levelTimeHighScores.getData(curLevelId) > time) {
                        textViewTimeRecord.setText("New record!");
                        Statistics.levelTimeHighScores.setData((long) time, curLevelId);
                    } else {
                        time = (int) (long) Statistics.levelTimeHighScores.getData(curLevelId);
                        hours = time / 3600000;
                        minutes = (time - hours * 3600000) / 60000;
                        seconds = (time - hours * 3600000 - minutes * 60000) / 1000;
                        ms = time - hours * 3600000 - minutes * 60000 - seconds * 1000;
                        textViewTimeRecord.setText("Record: " + (hours == 0 ? "" : (hours + "h ")) + (minutes == 0 ? "" : (minutes + "m ")) + seconds + "." + ms + "s");
                    }
                } else {
                    textViewTime.setText("Time : Skipped!");
                    time = (int) (long) Statistics.levelTimeHighScores.getData(curLevelId);
                    hours = time / 3600000;
                    minutes = (time - hours * 3600000) / 60000;
                    seconds = (time - hours * 3600000 - minutes * 60000) / 1000;
                    ms = time - hours * 3600000 - minutes * 60000 - seconds * 1000;
                    if (Statistics.levelTimeHighScores.getData(curLevelId) != -1 && Statistics.levelTimeHighScores.getData(curLevelId) != 1000000L) {
                        textViewTimeRecord.setText("Record: " + (hours == 0 ? "" : (hours + "h ")) + (minutes == 0 ? "" : (minutes + "m ")) + seconds + "." + ms + "s");
                    } else
                        textViewTimeRecord.setText("Record: ");
                    if (Statistics.levelTimeHighScores.getData(curLevelId) == -1) {
                        Statistics.levelTimeHighScores.setData(1000000L, curLevelId);
                    }
                }
                if (moveCount != -1) {
                    textViewMoves.setText("Moves : " + moveCount);
                    if (Statistics.levelMoveCountHighScores.getData(curLevelId) == -1 || Statistics.levelMoveCountHighScores.getData(curLevelId) > moveCount) {
                        textViewMovesRecord.setText("New record!");
                        Statistics.levelMoveCountHighScores.setData(moveCount, curLevelId);
                    } else {
                        textViewMovesRecord.setText("Record: " + Statistics.levelMoveCountHighScores.getData(curLevelId));
                    }
                } else {
                    textViewMoves.setText("Moves : Skipped!");
                    if (Statistics.levelMoveCountHighScores.getData(curLevelId) != -1 && Statistics.levelMoveCountHighScores.getData(curLevelId) != 1000000)
                        textViewMovesRecord.setText("Record: " + Statistics.levelMoveCountHighScores.getData(curLevelId));
                    else {
                        textViewMovesRecord.setText("Record: ");
                        Statistics.levelMoveCountHighScores.setData(1000000, curLevelId);
                    }
                }
            } else {

                textViewTime.setText("Time : " + (hours == 0 ? "" : (hours + "h ")) + (minutes == 0 ? "" : (minutes + "m ")) + seconds + "." + ms + "s");
                textViewMoves.setText("Moves : " + moveCount);
            }
            levelFinishWindow.setAnimationStyle(R.style.PopupAnimation);
            levelFinishWindow.showAtLocation(gameFrame, Gravity.CENTER, 0, 0);
            levelFinishWindow.setWindowLayoutMode(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            levelFinishWindow.update(0, 0, 300, 80);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            levelTimer.stop();
            if (towerTimer != null)
                towerTimer.cancel();
        } else {
            towerTimer.cancel();
            popupTowerFinish = new PopupWindow(this);
            final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(3, 3, 3, 3);
            LinearLayout llMain2 = new LinearLayout(this);
            llMain2.setBackgroundDrawable(getResources().getDrawable(R.drawable.popup_level_finish));
            llMain2.setOrientation(LinearLayout.VERTICAL);
            llMain2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            llMain2.setGravity(Gravity.CENTER);
            llMain2.setPadding(5, 5, 5, 5);

            TextView tV2 = new TextView(this);
            tV2.setTypeface(MainMenuActivity.addTypeface);
            tV2.setTextColor(getResources().getColor(R.color.flat_orange));
            tV2.setTextSize(34);
            llMain2.addView(tV2);
            tV2.setText("Well done!");
            tV2.setGravity(Gravity.CENTER);

            TextView textViewSkips = new TextView(this);
            textViewSkips.setTypeface(MainMenuActivity.addTypeface);
            textViewSkips.setTextColor(getResources().getColor(R.color.flat_white));
            textViewSkips.setTextSize(21);
            textViewSkips.setText("Tower completed");
            textViewSkips.setGravity(Gravity.CENTER);
            llMain2.addView(textViewSkips);

            Button buttonOk = new Button(context);
            buttonOk.setTypeface(MainMenuActivity.addTypeface);
            buttonOk = (Button)getLayoutInflater().inflate(getResources().getLayout(R.layout.template_level_select_button_green), null);
            buttonOk.setLayoutParams(params);
            buttonOk.setMinWidth(250);


            buttonOk.setTextSize(26);
            buttonOk.setText("OK");
            buttonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    long time;
                    dimBackground.setVisibility(View.GONE);
                    switch (TowerActivity.difficulty) {
                        case EASY: {
                            Statistics.easyTowersCompleted.setData(Statistics.easyTowersCompleted.getData().intValue() + 1);
                            time = Assets.easyTowerTime - (TowerActivity.getTimeLeft());
                            if (Statistics.easyTowersBestTime.getData() == -1 || Statistics.easyTowersBestTime.getData() > time) {
                                Statistics.easyTowersBestTime.setData(time);
                            }
                        }
                        break;
                        case MEDIUM: {
                            Statistics.mediumTowersCompleted.setData(Statistics.mediumTowersCompleted.getData().intValue() + 1);
                            time = Assets.mediumTowerTime - (TowerActivity.getTimeLeft());
                            if (Statistics.mediumTowersBestTime.getData() == -1 || Statistics.mediumTowersBestTime.getData() > time) {
                                Statistics.mediumTowersBestTime.setData(time);
                            }
                        }
                        break;
                        case HARD: {
                            Statistics.hardTowersCompleted.setData(Statistics.hardTowersCompleted.getData().intValue() + 1);
                            time = Assets.hardTowerTime - (TowerActivity.getTimeLeft());
                            if (Statistics.hardTowersBestTime.getData() == -1 || Statistics.hardTowersBestTime.getData() > time) {
                                Statistics.hardTowersBestTime.setData(time);
                            }
                        }
                        break;
                        case INFERNO: {
                            Statistics.infernoTowersCompleted.setData(Statistics.infernoTowersCompleted.getData().intValue() + 1);
                            time = Assets.infernoTowerTime - (TowerActivity.getTimeLeft());
                            if (Statistics.infernoTowersBestTime.getData() == -1 || Statistics.infernoTowersBestTime.getData() > time) {
                                Statistics.infernoTowersBestTime.setData(time);
                            }
                        }
                        break;
                    }
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    TowerActivity.reset();
                    popupTowerFinish.dismiss();
                    onLevelFail();
                }
            });
            llMain2.addView(buttonOk);
            popupTowerFinish.setContentView(llMain2);
            popupTowerFinish.setAnimationStyle(R.style.PopupAnimation);
            popupTowerFinish.showAtLocation(gameFrame, Gravity.CENTER, 0, 0);
            popupTowerFinish.setWindowLayoutMode(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            popupTowerFinish.update(0, 0, 300, 80);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
        Statistics.saveStatistics();
    }
    public void onLevelFail() {
        Intent intent = new Intent(this, GamemodeActivity.class);
        this.finish();
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE || newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            graph.transpose();
            canvasFrame.invalidate();
            transposeSize();
        }
    }
    private void transposeSize() {
        int temp = size.y;
        size.y = size.x;
        size.x = temp;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Chartboost.onStart(this);



    }
    @Override
    public void onResume() {
        super.onResume();
        Chartboost.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        Chartboost.onPause(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        Chartboost.onStop(this);
    }
    @Override
    public void onBackPressed() {
        if (Chartboost.onBackPressed())
            return;
        else
            super.onBackPressed();
    }

    private ChartboostDelegate chartboostDelegate = new ChartboostDelegate() {
        @Override
        public boolean shouldDisplayInterstitial(String location) {
            return super.shouldDisplayInterstitial(location);
        }
        @Override
        public void didDismissRewardedVideo(String location) {
        }
        @Override
        public void didCompleteRewardedVideo(String location, int reward) {
            Statistics.skipCount.setData(Statistics.skipCount.getData() + reward);
        }
        @Override
        public void didFailToLoadRewardedVideo(String location, CBError.CBImpressionError error) {
        }
        @Override
        public void didCloseInterstitial(String location) {
        }
    };
}

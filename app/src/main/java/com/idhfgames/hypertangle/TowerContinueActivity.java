package com.idhfgames.hypertangle;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class TowerContinueActivity extends Activity {
    Button buttonTowerContinue;
    Button buttonStartNew;
    private StartAppAd startAppAd = new StartAppAd(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            ActionBar actionBar = getActionBar();
            actionBar.hide();
        }
        StartAppSDK.init(this, getString(R.string.startapp_id), true);
        setContentView(R.layout.activity_tower_continue);
        buttonTowerContinue = (Button) findViewById(R.id.buttonTowerContinue);
        buttonStartNew = (Button) findViewById(R.id.buttonStartNew);
        buttonTowerContinue.setTypeface(MainMenuActivity.mainTypeface);
        buttonStartNew.setTypeface(MainMenuActivity.mainTypeface);
        startAppAd.showAd();
        startAppAd.loadAd();
    }
    public void onTowerStartNew(View view) {
        TowerActivity.graphs = null;
        Intent intent = new Intent(this, TowerDifficultyActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    public void onTowerContinue(View view) {
        Intent intent = new Intent(this, TowerActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    @Override
    public void onBackPressed() {
        this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    @Override
    public void onResume() {
        super.onResume();
        startAppAd.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        startAppAd.onPause();
    }
}

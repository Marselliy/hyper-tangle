package com.idhfgames.hypertangle;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.games.Games;
import com.idhfgames.hypertangle.core.MessagePopup;
import com.idhfgames.hypertangle.core.Statistics;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class StatsActivity extends Activity {
    TextView textTime;
    TextView textMoves;
    TextView textRandomLevels;
    TextView textViewTowerEasy;
    TextView textViewTowerMedium;
    TextView textViewTowerHard;
    TextView textViewTowerInferno;
    TextView textViewEasyTime;
    TextView textViewMediumTime;
    TextView textViewHardTime;
    TextView textViewInfernoTime;
    Button buttonReset;
    Button buttonSubmit;
    MessagePopup surePopup;
    RelativeLayout statsFrame;
    private StartAppAd startAppAd = new StartAppAd(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            ActionBar actionBar = getActionBar();
            actionBar.hide();
        }
        StartAppSDK.init(this, getString(R.string.startapp_id), true);
        setContentView(R.layout.activity_stats);
        textTime = (TextView) findViewById(R.id.textTime);
        textMoves = (TextView) findViewById(R.id.textMoves);
        textRandomLevels = (TextView) findViewById(R.id.textRandomLevels);
        buttonReset = (Button) findViewById(R.id.buttonReset);
        statsFrame = (RelativeLayout) findViewById(R.id.statsFrame);
        textViewTowerEasy = (TextView) findViewById(R.id.textViewTowerEasy);
        textViewTowerEasy.setText("\t" + getResources().getString(R.string.easy) + " : " + Statistics.easyTowersCompleted.getData());
        textViewTowerMedium = (TextView) findViewById(R.id.textViewTowerMedium);
        textViewTowerMedium.setText("\t" + getResources().getString(R.string.medium) + " : " + Statistics.mediumTowersCompleted.getData());
        textViewTowerHard = (TextView) findViewById(R.id.textViewTowerHard);
        textViewTowerHard.setText("\t" + getResources().getString(R.string.hard) + " : " + Statistics.hardTowersCompleted.getData());
        textViewTowerInferno = (TextView) findViewById(R.id.textViewTowerInferno);
        textViewTowerInferno.setText("\t" + getResources().getString(R.string.inferno) + " : " + Statistics.infernoTowersCompleted.getData());

        textViewEasyTime = (TextView) findViewById(R.id.textViewEasyTime);
        textViewMediumTime = (TextView) findViewById(R.id.textViewMediumTime);
        textViewHardTime = (TextView) findViewById(R.id.textViewHardTime);
        textViewInfernoTime = (TextView) findViewById(R.id.textViewInfernoTime);

        int time = (int) (long)Statistics.totalTime.getData();
        int hours = (int) (Statistics.totalTime.getData() / 3600000);
        int minutes = (int) (Statistics.totalTime.getData() - hours * 3600000) / 60000;
        int seconds = (int) (Statistics.totalTime.getData() - hours * 3600000 - minutes * 60000) / 1000;
        textTime.setText(getResources().getString(R.string.total_time) + " : " + hours + "h " + minutes + "m " + seconds + "s");
        textMoves.setText(getResources().getString(R.string.total_moves) + " : " + Statistics.moveCount.getData());
        textRandomLevels.setText(getResources().getString(R.string.random_levels_completed) + " : " + Statistics.randomLevelsCompleted.getData());


        time = Statistics.easyTowersBestTime.getData().intValue();
        if (time != -1) {
            hours = time / 3600000;
            minutes = (time - hours * 3600000) / 60000;
            seconds = (time - hours * 3600000 - minutes * 60000) / 1000;
            int ms = time - hours * 3600000 - minutes * 60000 - seconds * 1000;
            textViewEasyTime.setText("\t" + getResources().getString(R.string.easy) + " : " + (hours == 0 ? "" : (hours + "h ")) + (minutes == 0 ? "" : (minutes + "m ")) + seconds + (ms != 0 ? "." + ms : "") + "s");
        }
        time = Statistics.mediumTowersBestTime.getData().intValue();
        if (time != -1) {
            hours = time / 3600000;
            minutes = (time - hours * 3600000) / 60000;
            seconds = (time - hours * 3600000 - minutes * 60000) / 1000;
            int ms = time - hours * 3600000 - minutes * 60000 - seconds * 1000;
            textViewMediumTime.setText("\t" + getResources().getString(R.string.medium) + " : " + (hours == 0 ? "" : (hours + "h ")) + (minutes == 0 ? "" : (minutes + "m ")) + seconds + (ms != 0 ? "." + ms : "") + "s");
        }
        time = Statistics.hardTowersBestTime.getData().intValue();
        if (time != -1) {
            hours = time / 3600000;
            minutes = (time - hours * 3600000) / 60000;
            seconds = (time - hours * 3600000 - minutes * 60000) / 1000;
            int ms = time - hours * 3600000 - minutes * 60000 - seconds * 1000;
            textViewHardTime.setText("\t" + getResources().getString(R.string.hard) + " : " + (hours == 0 ? "" : (hours + "h ")) + (minutes == 0 ? "" : (minutes + "m ")) + seconds + (ms != 0 ? "." + ms : "") + "s");
        }
        time = Statistics.infernoTowersBestTime.getData().intValue();
        if (time != -1) {
            hours = time / 3600000;
            minutes = (time - hours * 3600000) / 60000;
            seconds = (time - hours * 3600000 - minutes * 60000) / 1000;
            int ms = time - hours * 3600000 - minutes * 60000 - seconds * 1000;
            textViewInfernoTime.setText("\t" + getResources().getString(R.string.inferno) + " : " + (hours == 0 ? "" : (hours + "h ")) + (minutes == 0 ? "" : (minutes + "m ")) + seconds + (ms != 0 ? "." + ms : "") + "s");
        }

        surePopup = new MessagePopup(this, "Reset?", null, "Yes", "No", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Statistics.resetStatistics();
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                surePopup.getPopup().dismiss();
                close();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                surePopup.getPopup().dismiss();
            }
        });

        if (MainMenuActivity.googleApiClient != null && MainMenuActivity.googleApiClient.isConnected()) {
            buttonSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSubmitButtonClick(v);
                }
            });
        }
        startAppAd.showAd();
        startAppAd.loadAd();
    }
    public void onResetClick(View view) {
        surePopup.getPopup().showAtLocation(statsFrame, Gravity.CENTER, 0, 0);
        surePopup.getPopup().setWindowLayoutMode(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        surePopup.getPopup().update(0, 0, 300, 80);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        TowerActivity.reset();
    }
    public void onSubmitButtonClick(View view) {
        Games.Leaderboards.submitScore(MainMenuActivity.googleApiClient, getString(R.string.leaderboard_total_play_time), Statistics.totalTime.getData());
        if (Statistics.randomLevelsCompleted.getData() != 0) {
            Games.Leaderboards.submitScore(MainMenuActivity.googleApiClient, getString(R.string.leaderboard_random_levels_completed), Statistics.randomLevelsCompleted.getData());
        }
        if (Statistics.easyTowersBestTime.getData() != -1) {
            Games.Leaderboards.submitScore(MainMenuActivity.googleApiClient, getString(R.string.leaderboard_easy_tower_best_time), Statistics.easyTowersBestTime.getData());
        }
        if (Statistics.mediumTowersBestTime.getData() != -1) {
            Games.Leaderboards.submitScore(MainMenuActivity.googleApiClient, getString(R.string.leaderboard_medium_tower_best_time), Statistics.mediumTowersBestTime.getData());
        }
        if (Statistics.hardTowersBestTime.getData() != -1) {
            Games.Leaderboards.submitScore(MainMenuActivity.googleApiClient, getString(R.string.leaderboard_hard_tower_best_time), Statistics.hardTowersBestTime.getData());
        }
        if (Statistics.infernoTowersBestTime.getData() != -1) {
            Games.Leaderboards.submitScore(MainMenuActivity.googleApiClient, getString(R.string.leaderboard_inferno_tower_best_time), Statistics.infernoTowersBestTime.getData());
        }
        onBackPressed();
    }
    public void close() {
        finish();
    }
    @Override
    public void onBackPressed() {
        close();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    @Override
    public void onResume() {
        super.onResume();
        startAppAd.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        startAppAd.onPause();
    }
}

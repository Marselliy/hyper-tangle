package com.idhfgames.hypertangle.core;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.idhfgames.hypertangle.MainMenuActivity;
import com.idhfgames.hypertangle.R;

/**
 * Created by sarik_000 on 06-Feb-16.
 */
public class MessagePopup extends Activity {
    private PopupWindow childPopup;
    public MessagePopup(Activity activity, String caption, String text, String leftText, View.OnClickListener leftAction) {
        childPopup = new PopupWindow();
        RelativeLayout rlFade = new RelativeLayout(activity);
        rlFade.setBackgroundColor(0xAA000000);
        LinearLayout llMain = new LinearLayout(activity);
        llMain.setBackgroundDrawable(MainMenuActivity.getContext().getResources().getDrawable(R.drawable.popup_level_finish));
        llMain.setGravity(Gravity.CENTER);
        llMain.setPadding(5, 5, 5, 5);
        llMain.setOrientation(LinearLayout.VERTICAL);
        rlFade.addView(llMain);
        RelativeLayout.LayoutParams layoutParams =
                (RelativeLayout.LayoutParams)llMain.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        llMain.setLayoutParams(layoutParams);

        if (caption != null) {
            TextView tVCaption = new TextView(activity);
            tVCaption.setTypeface(MainMenuActivity.addTypeface);
            tVCaption.setTextColor(MainMenuActivity.getContext().getResources().getColor(R.color.flat_white));
            tVCaption.setTextSize(34);
            tVCaption.setText(caption);
            llMain.addView(tVCaption);
        }
        if (text != null) {
            TextView tVText = new TextView(activity);
            tVText.setTypeface(MainMenuActivity.addTypeface);
            tVText.setTextColor(MainMenuActivity.getContext().getResources().getColor(R.color.flat_white));
            tVText.setTextSize(21);
            tVText.setText(text);
            llMain.addView(tVText);
        }

        if (leftText != null) {
            LayoutInflater inflater = (LayoutInflater) MainMenuActivity.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout llAdd = new LinearLayout(activity);
            llAdd.setOrientation(LinearLayout.HORIZONTAL);
            llAdd.setGravity(Gravity.LEFT);
            llAdd.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            llMain.addView(llAdd);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(3, 3, 3, 3);
            Button buttonLeft = new Button(activity);
            buttonLeft.setTypeface(MainMenuActivity.addTypeface);
            buttonLeft.setTextColor(MainMenuActivity.getContext().getResources().getColor(R.color.flat_white));
            buttonLeft = (Button) inflater.inflate(R.layout.template_level_select_button_green, null);
            buttonLeft.setTextSize(21);
            buttonLeft.setPadding(8, 8, 8, 8);
            llAdd.addView(buttonLeft);
            buttonLeft.setLayoutParams(params);
            buttonLeft.setText(leftText);
            buttonLeft.setOnClickListener(leftAction);
            buttonLeft.setMinWidth(300);
        }
        childPopup.setContentView(rlFade);

    }
    public MessagePopup(Activity activity, String caption, String text, String leftText, String rightText, View.OnClickListener leftAction, View.OnClickListener rightAction) {
        childPopup = new PopupWindow();
        RelativeLayout rlFade = new RelativeLayout(activity);
        rlFade.setBackgroundColor(0xAA000000);
        LinearLayout llMain = new LinearLayout(activity);
        llMain.setBackgroundDrawable(MainMenuActivity.getContext().getResources().getDrawable(R.drawable.popup_level_finish));
        llMain.setGravity(Gravity.CENTER);
        llMain.setPadding(5, 5, 5, 5);
        llMain.setOrientation(LinearLayout.VERTICAL);
        rlFade.addView(llMain);
        RelativeLayout.LayoutParams layoutParams =
                (RelativeLayout.LayoutParams)llMain.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        llMain.setLayoutParams(layoutParams);

        if (caption != null) {
            TextView tVCaption = new TextView(activity);
            tVCaption.setTypeface(MainMenuActivity.addTypeface);
            tVCaption.setTextColor(MainMenuActivity.getContext().getResources().getColor(R.color.flat_white));
            tVCaption.setTextSize(34);
            tVCaption.setText(caption);
            llMain.addView(tVCaption);
        }
        if (text != null) {
            TextView tVText = new TextView(activity);
            tVText.setTypeface(MainMenuActivity.addTypeface);
            tVText.setTextColor(MainMenuActivity.getContext().getResources().getColor(R.color.flat_white));
            tVText.setTextSize(21);
            tVText.setText(text);
            llMain.addView(tVText);
        }
        //    tV.setText("Level completed!");

        if (leftText != null || rightText != null) {
            LayoutInflater inflater = (LayoutInflater) MainMenuActivity.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout llAdd = new LinearLayout(activity);
            llAdd.setOrientation(LinearLayout.HORIZONTAL);
            llAdd.setGravity(Gravity.LEFT);
            llAdd.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            llMain.addView(llAdd);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(3, 3, 3, 3);

            if (leftText != null) {
                Button buttonLeft = new Button(activity);
                buttonLeft.setTypeface(MainMenuActivity.addTypeface);
                buttonLeft.setTextColor(MainMenuActivity.getContext().getResources().getColor(R.color.flat_white));
                buttonLeft = (Button) inflater.inflate(R.layout.template_level_select_button_green, null);
                buttonLeft.setTextSize(21);
                buttonLeft.setPadding(8, 8, 8, 8);
                llAdd.addView(buttonLeft);
                buttonLeft.setLayoutParams(params);
                buttonLeft.setText(leftText);
                buttonLeft.setOnClickListener(leftAction);
            }
            if (rightText != null) {
                Button buttonRight = new Button(activity);
                buttonRight.setTypeface(MainMenuActivity.addTypeface);
                buttonRight.setTextColor(MainMenuActivity.getContext().getResources().getColor(R.color.flat_white));
                buttonRight = (Button) inflater.inflate(R.layout.template_level_select_button_red, null);
                buttonRight.setTextSize(21);
                buttonRight.setPadding(8, 8, 8, 8);
                llAdd.addView(buttonRight);
                buttonRight.setLayoutParams(params);
                buttonRight.setText(rightText);
                buttonRight.setOnClickListener(rightAction);
            }
        }
        childPopup.setContentView(rlFade);

    }
    public MessagePopup(Context context, String caption, String text, String leftText, View.OnClickListener leftAction) {
        childPopup = new PopupWindow();
        RelativeLayout rlFade = new RelativeLayout(context);
        rlFade.setBackgroundColor(0xAA000000);
        LinearLayout llMain = new LinearLayout(context);
        llMain.setBackgroundDrawable(MainMenuActivity.getContext().getResources().getDrawable(R.drawable.popup_level_finish));
        llMain.setGravity(Gravity.CENTER);
        llMain.setPadding(5, 5, 5, 5);
        llMain.setOrientation(LinearLayout.VERTICAL);
        rlFade.addView(llMain);
        RelativeLayout.LayoutParams layoutParams =
                (RelativeLayout.LayoutParams)llMain.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        llMain.setLayoutParams(layoutParams);

        if (caption != null) {
            TextView tVCaption = new TextView(context);
            tVCaption.setTypeface(MainMenuActivity.addTypeface);
            tVCaption.setTextColor(MainMenuActivity.getContext().getResources().getColor(R.color.flat_white));
            tVCaption.setTextSize(34);
            tVCaption.setText(caption);
            llMain.addView(tVCaption);
        }
        if (text != null) {
            TextView tVText = new TextView(context);
            tVText.setTypeface(MainMenuActivity.addTypeface);
            tVText.setTextColor(MainMenuActivity.getContext().getResources().getColor(R.color.flat_white));
            tVText.setTextSize(21);
            tVText.setText(text);
            llMain.addView(tVText);
        }

        if (leftText != null) {
            LayoutInflater inflater = (LayoutInflater) MainMenuActivity.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout llAdd = new LinearLayout(context);
            llAdd.setOrientation(LinearLayout.HORIZONTAL);
            llAdd.setGravity(Gravity.LEFT);
            llAdd.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            llMain.addView(llAdd);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(3, 3, 3, 3);
            Button buttonLeft = new Button(context);
            buttonLeft.setTypeface(MainMenuActivity.addTypeface);
            buttonLeft.setTextColor(MainMenuActivity.getContext().getResources().getColor(R.color.flat_white));
            buttonLeft = (Button) inflater.inflate(R.layout.template_level_select_button_green, null);
            buttonLeft.setTextSize(21);
            buttonLeft.setPadding(8, 8, 8, 8);
            llAdd.addView(buttonLeft);
            buttonLeft.setLayoutParams(params);
            buttonLeft.setText(leftText);
            buttonLeft.setOnClickListener(leftAction);
            buttonLeft.setMinWidth(300);
        }
        childPopup.setContentView(rlFade);
    }
    public MessagePopup(Context context, String caption, String text, String leftText, String rightText, View.OnClickListener leftAction, View.OnClickListener rightAction) {
        childPopup = new PopupWindow();
        RelativeLayout rlFade = new RelativeLayout(context);
        rlFade.setBackgroundColor(0xAA000000);
        LinearLayout llMain = new LinearLayout(context);
        llMain.setBackgroundDrawable(MainMenuActivity.getContext().getResources().getDrawable(R.drawable.popup_level_finish));
        llMain.setGravity(Gravity.CENTER);
        llMain.setPadding(5, 5, 5, 5);
        llMain.setOrientation(LinearLayout.VERTICAL);
        rlFade.addView(llMain);
        RelativeLayout.LayoutParams layoutParams =
                (RelativeLayout.LayoutParams)llMain.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        llMain.setLayoutParams(layoutParams);

        if (caption != null) {
            TextView tVCaption = new TextView(context);
            tVCaption.setTypeface(MainMenuActivity.addTypeface);
            tVCaption.setTextColor(MainMenuActivity.getContext().getResources().getColor(R.color.flat_white));
            tVCaption.setTextSize(34);
            tVCaption.setText(caption);
            llMain.addView(tVCaption);
        }
        if (text != null) {
            TextView tVText = new TextView(context);
            tVText.setTypeface(MainMenuActivity.addTypeface);
            tVText.setTextColor(MainMenuActivity.getContext().getResources().getColor(R.color.flat_white));
            tVText.setTextSize(21);
            tVText.setText(text);
            llMain.addView(tVText);
        }

        if (leftText != null || rightText != null) {
            LayoutInflater inflater = (LayoutInflater) MainMenuActivity.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout llAdd = new LinearLayout(context);
            llAdd.setOrientation(LinearLayout.HORIZONTAL);
            llAdd.setGravity(Gravity.LEFT);
            llAdd.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            llMain.addView(llAdd);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(3, 3, 3, 3);

            if (leftText != null) {
                Button buttonLeft = new Button(context);
                buttonLeft.setTypeface(MainMenuActivity.addTypeface);
                buttonLeft.setTextColor(MainMenuActivity.getContext().getResources().getColor(R.color.flat_white));
                buttonLeft = (Button) inflater.inflate(R.layout.template_level_select_button_green, null);
                buttonLeft.setTextSize(21);
                buttonLeft.setPadding(8, 8, 8, 8);
                llAdd.addView(buttonLeft);
                buttonLeft.setLayoutParams(params);
                buttonLeft.setText(leftText);
                buttonLeft.setOnClickListener(leftAction);
            }
            if (rightText != null) {
                Button buttonRight = new Button(context);
                buttonRight.setTypeface(MainMenuActivity.addTypeface);
                buttonRight.setTextColor(MainMenuActivity.getContext().getResources().getColor(R.color.flat_white));
                buttonRight = (Button) inflater.inflate(R.layout.template_level_select_button_red, null);
                buttonRight.setTextSize(21);
                buttonRight.setPadding(8, 8, 8, 8);
                llAdd.addView(buttonRight);
                buttonRight.setLayoutParams(params);
                buttonRight.setText(rightText);
                buttonRight.setOnClickListener(rightAction);
            }
        }
        childPopup.setContentView(rlFade);
    }
    public PopupWindow getPopup() {
        return childPopup;
    }
}

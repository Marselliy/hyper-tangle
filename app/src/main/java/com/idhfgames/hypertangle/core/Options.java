package com.idhfgames.hypertangle.core;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by sarik_000 on 01.08.2015.
 */
public class Options {
    private static Context context;
    private static SharedPreferences preferences;

    public static boolean realTimeIntersections;

    public static void loadOptions(Context context, SharedPreferences preferences) {
        Options.context = context;
        Options.preferences = preferences;

        realTimeIntersections = preferences.getBoolean("realTimeIntersections", false);
    }
    public static void saveOptions() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("realTimeIntersections", realTimeIntersections);
        editor.commit();
    }
}

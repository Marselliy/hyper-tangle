package com.idhfgames.hypertangle.core;

/**
 * Created by sarik_000 on 25.07.2015.
 */
public class Assets {
    public static int easyLevelNum = 50;
    public static int mediumLevelNum = 50;
    public static int hardLevelNum = 50;
    public static int infernoLevelNum = 150;
    public static long easyTowerTime = 600000;
    public static long mediumTowerTime = 900000;
    public static long hardTowerTime = 1200000;
    public static long infernoTowerTime = 1500000;
    public static int getTotalLevelNum() {
        return easyLevelNum + mediumLevelNum + hardLevelNum + infernoLevelNum;
    }
}

package com.idhfgames.hypertangle.core;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by sarik_000 on 31.07.2015.
 */
public class AchievmentView extends View{
    public AchievmentView(Context context) {
        super(context);
    }

    public AchievmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AchievmentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}

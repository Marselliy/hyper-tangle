package com.idhfgames.hypertangle.core;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.idhfgames.hypertangle.R;

import java.util.ArrayList;

/**
 * Created by sarik_000 on 27.07.2015.
 */
public class CanvasFrame extends FrameLayout {

    Graph graph;
    Paint paintFill;
    Paint paintStroke;
    Paint paintNodeFillGood;
    Paint paintNodeFillBad;
    Paint paintStrokeGood;
    Paint paintStrokeBad;
    Paint paintFillGood;
    Paint paintFillBad;
    public ArrayList<Integer> problemLinks;
    public ArrayList<Integer> problemNodes;

    public CanvasFrame(Context context, Graph graph) {
        super(context);
        this.graph = graph;
        paintStroke = new Paint();
        paintStroke.setStrokeWidth(2f);
        paintStroke.setStyle(Paint.Style.STROKE);
        paintStroke.setColor(Color.argb(255, 30, 30, 30));

        paintStrokeGood = new Paint();
        paintStrokeGood.setStrokeWidth(2f);
        paintStrokeGood.setStyle(Paint.Style.STROKE);
        paintStrokeGood.setColor(getResources().getColor(R.color.flat_green));

        paintStrokeBad = new Paint();
        paintStrokeBad.setStrokeWidth(2f);
        paintStrokeBad.setStyle(Paint.Style.STROKE);
        paintStrokeBad.setColor(getResources().getColor(R.color.flat_red));

        paintFillGood = new Paint();
        paintFillGood.setStrokeWidth(2f);
        paintFillGood.setStyle(Paint.Style.FILL);
        paintFillGood.setColor(getResources().getColor(R.color.flat_green_transp));

        paintFillBad = new Paint();
        paintFillBad.setStrokeWidth(2f);
        paintFillBad.setStyle(Paint.Style.FILL);
        paintFillBad.setColor(getResources().getColor(R.color.flat_red_transp));

        paintFill = new Paint();
        paintFill.setStrokeWidth(2f);
        paintFill.setStyle(Paint.Style.FILL);
        paintFill.setColor(Color.argb(30, 0, 0, 0));

        paintNodeFillGood = new Paint();
        paintNodeFillGood.setStrokeWidth(2f);
        paintNodeFillGood.setStyle(Paint.Style.FILL);
        paintNodeFillGood.setColor(getResources().getColor(R.color.flat_green_dark));

        paintNodeFillBad = new Paint();
        paintNodeFillBad.setStrokeWidth(2f);
        paintNodeFillBad.setStyle(Paint.Style.FILL);
        paintNodeFillBad.setColor(getResources().getColor(R.color.flat_red_dark));

        float[] colorMatrixArray = {1, 1, 1, 1, 0,
                0, 0, 0, 0, 0,
                0, 0, 0, 0, 0,
                0, 0, 0, 1, 0 };
        ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrixArray);
        paintFill.setColorFilter(colorFilter);
        setWillNotDraw(false);

//        test = new boolean[300][300];

    }

    public CanvasFrame(Context context, AttributeSet attrs, Graph graph) {
        super(context, attrs);
        this.graph = graph;
    }

    public CanvasFrame(Context context, AttributeSet attrs, int defStyleAttr, Graph graph) {
        super(context, attrs, defStyleAttr);
        this.graph = graph;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (int i = 0; i < graph.getIncMatrix().length; i++) {
            Path linkPath = new Path();
            ArrayList<Point> pointsToLink = graph.getLinkConvexHull(i);
            boolean flag = false;
            for (Point p1 : pointsToLink) {
                if (flag)
                    linkPath.lineTo(p1.x , p1.y);
                else
                    linkPath.moveTo(p1.x, p1.y);
                flag = true;
            }
            linkPath.close();
            if (problemLinks != null && problemLinks.contains(i)) {
                canvas.drawPath(linkPath, paintStrokeBad);
                canvas.drawPath(linkPath, paintFillBad);
            } else {
                canvas.drawPath(linkPath, paintStrokeGood);
                canvas.drawPath(linkPath, paintFillGood);
            }
        }
        for (int i = 0; i < graph.nodes.length; i++) {
            if (problemNodes != null && problemNodes.contains(i)) {
                canvas.drawCircle(graph.nodes[i].getX() + graph.getNodeSize() / 2, graph.nodes[i].getY() + graph.getNodeSize() / 2, 16, paintNodeFillBad);
                canvas.drawCircle(graph.nodes[i].getX() + graph.getNodeSize() / 2, graph.nodes[i].getY() + graph.getNodeSize() / 2, 16, paintStrokeBad);
            }
            else {
                canvas.drawCircle(graph.nodes[i].getX() + graph.getNodeSize() / 2, graph.nodes[i].getY() + graph.getNodeSize() / 2, 16, paintNodeFillGood);
                canvas.drawCircle(graph.nodes[i].getX() + graph.getNodeSize() / 2, graph.nodes[i].getY() + graph.getNodeSize() / 2, 16, paintStrokeGood);
            }
        }
    }
}

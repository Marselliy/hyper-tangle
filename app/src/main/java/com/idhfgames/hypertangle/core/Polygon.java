package com.idhfgames.hypertangle.core;

/**
 * Created by sarik_000 on 29.07.2015.
 */


/**
 * Minimum Polygon class for Android.
 */
public class Polygon {

    // Polygon coodinates.
    private int[] polyY, polyX;

    // Number of sides in the polygon.
    private int polySides;

    /**
     * Default constructor.
     * @param px Polygon y coods.
     * @param py Polygon x coods.
     * @param ps Polygon sides count.
     */
    public Polygon( int[] px, int[] py, int ps ) {

        polyX = px;
        polyY = py;
        polySides = ps;
    }

    /**
     * Checks if the Polygon contains a point.
     * @see "http://alienryderflex.com/polygon/"
     * @param x Point horizontal pos.
     * @param y Point vertical pos.
     * @return Point is in Poly flag.
     */
    public boolean contains( int x, int y ) {

        int i;
        int j;
        boolean result = false;
        for (i = 0, j = polySides - 1; i < polySides; j = i++) {
            if ((polyY[i] > y) != (polyY[j] > y) &&
                    (x < (polyX[j] - polyX[i]) * (y - polyY[i]) / (polyY[j]-polyY[i]) + polyX[i])) {
                result = !result;
            }
        }
        return result;
    }
}
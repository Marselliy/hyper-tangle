package com.idhfgames.hypertangle.core;

import android.graphics.Point;
import android.util.Pair;
import android.view.Display;
import android.widget.Button;
import com.idhfgames.hypertangle.MainMenuActivity;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by sarik_000 on 23.07.2015.
 */
public class Graph {
    private int nodeNum;
    private int linkNum;
    private boolean[][] incMatrix;
    private Point[] nodeCoords;
    private Point[] nodesSolution;
    private int nodeSize = 64;
    public Button[] nodes;

    public enum TYPE {
        SCATTERED,
        NONSCATTERED
    }

    public Graph() {

    }

    public Graph(String data) {

        String[] parts = data.split(" ");
        nodeNum = Integer.parseInt(parts[0]);
        linkNum = Integer.parseInt(parts[1]);
        incMatrix = new boolean[linkNum][];
        for (int i = 0; i < incMatrix.length; i++) {
            incMatrix[i] = new boolean[nodeNum];
            for (int j = 0; j < incMatrix[i].length; j++) {
                incMatrix[i][j] = parts[2].charAt(i * nodeNum + j) != '0';
            }
        }
        Display display = MainMenuActivity.display;
        Point size = new Point();
        display.getSize(size);
        if (parts.length == 5) {
            String[] partsSolution = parts[4].split("-");
            nodesSolution = new Point[partsSolution.length / 2];
            for (int i = 0; i < nodesSolution.length; i++) {
                nodesSolution[i] = new Point((int) (size.x * Float.parseFloat(partsSolution[2 * i])), (int) (size.y * Float.parseFloat(partsSolution[2 * i + 1])));
            }
        }
        parts = parts[3].split("-");
        nodeCoords = new Point[parts.length / 2];
        for (int i = 0; i < nodeCoords.length; i++) {
            nodeCoords[i] = new Point((int) (size.x * Float.parseFloat(parts[2 * i])), (int) (size.y * Float.parseFloat(parts[2 * i + 1])));
        }
        nodeSize = (int) (MainMenuActivity.dm.densityDpi * 0.4);
        nodes = new Button[nodeCoords.length];
    }

    public static Graph getRandomGraph(int difficulty) {
        Graph graph = new Graph();
        Random random = new Random();
        graph.nodeNum = random.nextInt(5) + difficulty;
        graph.linkNum = random.nextInt(5) + difficulty;
        graph.incMatrix = new boolean[graph.linkNum][];
        for (int i = 0; i < graph.incMatrix.length; i++) {
            graph.incMatrix[i] = new boolean[graph.nodeNum];
            for (int j = 0; j < graph.incMatrix[i].length; j++) {
                graph.incMatrix[i][j] = random.nextInt() % 3 == 0;
            }
        }
        Display display = MainMenuActivity.display;
        Point size = new Point();
        display.getSize(size);
        graph.nodeCoords = new Point[graph.nodeNum];
        for (int i = 0; i < graph.nodeCoords.length; i++) {
            graph.nodeCoords[i] = new Point((int) (size.x * (random.nextFloat() * 0.6 + 0.2)), (int) (size.y * (random.nextFloat() * 0.6 + 0.2)));
        }
        graph.nodes = new Button[graph.nodeCoords.length];
        return graph;
    }

    public static Graph getRandomGraph(int dirCount, int nodeCount, TYPE type) {
        nodeCount += dirCount - 1;
        Random random = new Random();
        Graph graph = new Graph();
        int[] dirLengths = new int[dirCount];
        switch (type) {
            case NONSCATTERED: {
                for (int i = 0; i < nodeCount; i++) {
                    dirLengths[i % dirCount]++;
                }
            }
            break;
            case SCATTERED: {

            }
            break;
        }
        int[][] nodesMatr = new int[dirCount][];
        for (int i = 0; i < nodesMatr.length; i++) {
            nodesMatr[i] = new int[dirLengths[i]];
        }
        int counter = 1;
        for (int i = 0; i < nodesMatr.length; i++) {
            nodesMatr[i][0] = 0;
            for (int j = 1; j < nodesMatr[i].length; j++) {
                nodesMatr[i][j] = counter++;
            }
        }

        ArrayList<int[]> nodeList = new ArrayList<int[]>();
        for (int i = 0; i < nodesMatr.length; i++) {
            for (int j = 0; j < nodesMatr[i].length - 1; j++) {
                nodeList.add(new int[]{nodesMatr[i][j], nodesMatr[i][j + 1]});
            }
        }

        graph.nodesSolution = new Point[nodeCount];
        for (int i = 0; i < nodesMatr.length; i++) {
            for (int j = 0; j < nodesMatr[i].length; j++) {
                graph.nodesSolution[nodesMatr[i][j]] = new Point((int) (0.5 + Math.cos(2 * Math.PI * i / nodesMatr.length) * (0.4 * j / nodesMatr[i].length)), (int) (0.5 + Math.sin(2 * Math.PI * i / nodesMatr.length) * (0.4 * j / nodesMatr[i].length)));
            }
        }

        for (int i = 0; i < nodesMatr.length; i++) {
            int[] node1 = nodesMatr[i];
            int[] node2 = nodesMatr[(i + 1) % nodesMatr.length];
            for (int j = 0; j < Math.max(node1.length, node2.length) - 1; j++) {
                if (j + 1 < node1.length && j + 1 < node2.length) {
                    switch (random.nextInt(7)) {
                        case 0: {
                            nodeList.add(new int[]{node1[j], node1[j + 1], node2[j], node2[j + 1]});
                        }
                        break;
                        case 1: {
                            switch (random.nextInt(2)) {
                                case 0: {
                                    nodeList.add(new int[]{node1[j], node2[j]});
                                    nodeList.add(new int[]{node1[j], node1[j + 1], node2[j + 1]});
                                }
                                break;
                                case 1: {
                                    nodeList.add(new int[]{node1[j], node2[j]});
                                    nodeList.add(new int[]{node2[j], node1[j + 1], node2[j + 1]});
                                }
                                break;
                            }
                        }
                        break;
                        case 2: {
                            switch (random.nextInt(2)) {
                                case 0: {
                                    nodeList.add(new int[]{node1[j], node2[j + 1], node2[j]});
                                    nodeList.add(new int[]{node1[j], node1[j + 1], node2[j + 1]});
                                }
                                break;
                                case 1: {
                                    nodeList.add(new int[]{node1[j], node2[j]});
                                    nodeList.add(new int[]{node2[j], node1[j + 1], node2[j + 1]});
                                }
                                break;
                            }
                        }
                        break;
                        case 3: {
                            switch (random.nextInt(2)) {
                                case 0: {
                                    nodeList.add(new int[]{node1[j], node2[j]});
                                    nodeList.add(new int[]{node1[j], node2[j + 1]});
                                }
                                break;
                                case 1: {
                                    nodeList.add(new int[]{node1[j], node2[j]});
                                    nodeList.add(new int[]{node2[j], node1[j + 1]});
                                }
                                break;
                            }
                        }
                        break;
                        case 4: {
                            switch (random.nextInt(2)) {
                                case 0: {
                                    nodeList.add(new int[]{node1[j], node2[j + 1]});
                                }
                                break;
                                case 1: {
                                    nodeList.add(new int[]{node2[j], node1[j + 1]});
                                }
                                break;
                            }
                        }
                        break;
                        case 5: {
                            nodeList.add(new int[]{node1[j], node2[j]});
                        }
                        break;
                    }
                } else if (j + 1 == node1.length && j + 1 < node2.length) {
                    switch (random.nextInt(5)) {
                        case 0: {
                            nodeList.add(new int[]{node1[j], node2[j + 1]});
                        }
                        break;
                        case 1: {
                            nodeList.add(new int[]{node1[j], node2[j]});
                        }
                        break;
                        case 2: {
                            nodeList.add(new int[]{node1[j], node2[j]});
                            nodeList.add(new int[]{node1[j], node2[j + 1]});
                        }
                        break;
                        case 3: {
                            nodeList.add(new int[]{node1[j], node2[j], node2[j + 1]});
                        }
                        break;
                    }
                } else if (j + 1 < node1.length && j + 1 == node2.length) {
                    switch (random.nextInt(5)) {
                        case 0: {
                            nodeList.add(new int[]{node2[j], node1[j + 1]});
                        }
                        break;
                        case 1: {
                            nodeList.add(new int[]{node2[j], node1[j]});
                        }
                        break;
                        case 2: {
                            nodeList.add(new int[]{node2[j], node1[j]});
                            nodeList.add(new int[]{node2[j], node1[j + 1]});
                        }
                        break;
                        case 3: {
                            nodeList.add(new int[]{node2[j], node1[j], node1[j + 1]});
                        }
                        break;
                    }
                } else if (j + 1 == node1.length && j + 1 == node2.length) {
                    switch (random.nextInt(2)) {
                        case 0: {
                            nodeList.add(new int[]{node1[j], node2[j]});
                        }
                        break;
                    }
                }
            }
        }
        graph.linkNum = nodeList.size();
        graph.nodeNum = counter;
        graph.incMatrix = new boolean[nodeList.size()][counter];
        for (int i = 0; i < nodeList.size(); i++) {
            for (int j = 0; j < nodeList.get(i).length; j++) {
                graph.incMatrix[i][nodeList.get(i)[j]] = true;
            }
        }

        graph.nodes = new Button[graph.nodeNum];
        int flag = 0;
        if (graph.nodeNum > 7)
            flag = random.nextInt(4);
        switch (flag) {
            case 0: {
                graph.nodeCoords = Graph.getRandomCoords(graph.nodeNum);
            }
            break;
            case 1: {
                graph.nodeCoords = Graph.getSpiralCoords(graph.nodeNum);
            }
            break;
            case 2: {
                graph.nodeCoords = Graph.getCircleCoords(graph.nodeNum);
            }
            break;
            default: {
                graph.nodeCoords = Graph.getRandomSymmCoords(graph.nodeNum);
            }
        }


        return graph;
    }

    public Pair<ArrayList<Integer>, ArrayList<Integer>> isRealized() {
        ArrayList<Integer> problemLinks = new ArrayList<>();
        ArrayList<Integer> problemNodes = new ArrayList<>();
        problemNodes = new ArrayList<>();
        for (int i = 0; i < getIncMatrix().length; i++) {
            ArrayList<Point> pointsToLink = getLinkConvexHull(i);
            int[] x = new int[pointsToLink.size()];
            int[] y = new int[pointsToLink.size()];
            for (int j = 0; j < pointsToLink.size(); j++) {
                x[j] = pointsToLink.get(j).x;
                y[j] = pointsToLink.get(j).y;
            }
            Polygon polygon = new Polygon(x, y, pointsToLink.size());
            for (int j = 0; j < nodeNum; j++) {
                if (!incMatrix[i][j] && polygon.contains((int) nodes[j].getX() + nodeSize / 2, (int) nodes[j].getY() + nodeSize / 2)) {
                    if (!problemLinks.contains(i)) {
                        problemLinks.add(i);
                        for (int k = 0; k < getIncMatrix().length; k++) {
                            if (getIncMatrix()[k][j] && !pointsToLink.contains(k))
                                problemLinks.add(k);
                        }
                    }
                    if (!problemNodes.contains(j)) {
                        problemNodes.add(j);
                    }
                }
            }
        }
        for (int i = 0; i < linkNum; i++) {
            for (int j = 0; j < i; j++) {
                ArrayList<Point> link1 = getLinkConvexHull(i);
                ArrayList<Point> link2 = getLinkConvexHull(j);
                int size1 = link1.size();
                int size2 = link2.size();
                if (size1 > 1 && size2 > 1) {
                    for (int p = -1; p < size1; p++) {
                        for (int q = -1; q < size2; q++) {
                            if (LineIntersection.check(link1.get((p + size1) % size1), link1.get((p + 1) % size1), link2.get((q + size2) % size2), link2.get((q + 1) % size2))) {
                                if (!problemLinks.contains(i))
                                    problemLinks.add(i);
                                if (!problemLinks.contains(j))
                                    problemLinks.add(j);
                            }
                        }
                    }
                }
            }
        }
        for (Integer link : problemLinks) {
            for (int i = 0; i < getNodeNum(); i++) {
                if (getIncMatrix()[link][i] && !problemNodes.contains(i)) {
                    problemNodes.add(i);
                }
            }
        }
        return new Pair<>(problemLinks, problemNodes);
    }

    public int getNodeNum() {
        return nodeNum;
    }

    public int getLinkNum() {
        return linkNum;
    }

    public boolean[][] getIncMatrix() {
        return incMatrix;
    }

    public Point[] getNodeCoords() {
        return nodeCoords;
    }

    public int getNodeSize() {
        return nodeSize;
    }

    public boolean isPlanar() {
        boolean[][] LMatrix = new boolean[nodeNum + linkNum][nodeNum + linkNum];
        for (int i = 0; i < incMatrix.length; i++) {
            for (int j = 0; j < incMatrix[i].length; j++) {
                LMatrix[j + linkNum][i] = LMatrix[i][j + linkNum] = incMatrix[i][j];
            }
        }
        return false;
    }

    public ArrayList<Point> getLinkConvexHull(int index) {
        ArrayList<Point> pointsToLink = new ArrayList<>();
        for (int j = 0; j < incMatrix[index].length; j++) {
            if (incMatrix[index][j]) {
                pointsToLink.add(new Point((int) nodes[j].getX() + nodeSize / 2, (int) nodes[j].getY() + nodeSize / 2));
            }
        }
        if (pointsToLink.size() > 3)
            pointsToLink = ConvexHull.quickHull(pointsToLink);
        return pointsToLink;
    }

    public String toString() {
        String data = nodeNum + " " + linkNum + " ";
        for (int i = 0; i < linkNum; i++) {
            for (int j = 0; j < nodeNum; j++) {
                data += (incMatrix[j][i] ? "1" : "0");
            }
        }
        data += " ";
        for (int i = 0; i < nodes.length; i++) {
            data += nodes[i].getX() + "-" + nodes[i].getY() + "-";
        }
        data = data.substring(0, data.length() - 1);
        return data;
    }

    public int getDifficulty() {
        int dif = 0;
        for (int i = 0; i < incMatrix.length; i++) {
            for (int j = 0; j < incMatrix[i].length; j++) {
                if (incMatrix[i][j])
                    dif++;
            }
        }
        return dif;
    }

    public void applySolution() {
        for (int i = 0; i < nodeCoords.length; i++) {
            nodes[i].setX(nodesSolution[i].x - nodeSize / 2);
            nodes[i].setY(nodesSolution[i].y - nodeSize / 2);
        }
    }

    private static Point[] getRandomCoords(int count) {
        Random random = new Random();
        Point[] nodeArr = new Point[count];
        Point size = new Point();
        MainMenuActivity.display.getSize(size);

        for (int i = 0; i < nodeArr.length; i++) {
            if (i < 9) {
                nodeArr[i] = new Point((int) (size.x * (random.nextFloat() * 0.4 + 0.2 * (1 + (double) (i / 3) / 2))), (int) (size.y * (random.nextFloat() * 0.4 + 0.2 * (1 + (double) (i % 3) / 2))));
            } else {
                nodeArr[i] = new Point((int) (size.x * (random.nextFloat() * 0.6 + 0.2)), (int) (size.y * (random.nextFloat() * 0.6 + 0.2)));
            }
        }
        return nodeArr;
    }
    private static Point[] getRandomSymmCoords(int count) {
        if (count < 7)
            return getRandomCoords(count);
        double aspectRatio = 480.0 / 800;
        Random random = new Random();
        Point[] nodeArr = new Point[count];
        Point size = new Point();
        MainMenuActivity.display.getSize(size);

        ArrayList<Integer> parts = new ArrayList<Integer>();
        int countTemp = count;
        while (countTemp > 0) {
            int part;
            switch (random.nextInt(5)) {
                case 1: {
                    part = 2;
                } break;
                case 2: {
                    part = 3;
                } break;
                case 3: {
                    part = 4;
                } break;
                case 4: {
                    part = 6;
                } break;
                default: {
                    part = 1;
                }
            }
            if (countTemp - part >= 0) {
                parts.add(part);
                countTemp -= part;
            }
        }

        int i = 0;
        for (Integer part : parts) {
            switch (part) {
                case 1: {
                    nodeArr[i] = new Point((int)(size.x * (random.nextFloat() * 0.6 + 0.2)), (int)(size.y * (random.nextFloat() * 0.6 + 0.2)));
                    i++;
                } break;
                case 2: {
                    float dx = random.nextFloat() * 0.8f - 0.4f;
                    float dy = random.nextFloat() * 0.8f - 0.4f;
                    nodeArr[i] = new Point((int)(size.x * (0.5 + dx)), (int)(size.y * (0.5 + dy)));
                    nodeArr[i + 1] = new Point((int)(size.x * (0.5 - dx)), (int)(size.y * (0.5 + dy)));
                    i += 2;
                } break;
                case 3: {
                    float radius = random.nextFloat() * 0.4f;
                    nodeArr[i    ] = new Point((int)(size.x * (0.5 + radius * Math.cos(Math.PI * 0 / 180))), (int)(size.y * (0.5 + radius * aspectRatio * Math.sin(Math.PI * 0 / 180))));
                    nodeArr[i + 1] = new Point((int)(size.x * (0.5 + radius * Math.cos(Math.PI * 120 / 180))), (int)(size.y * (0.5 + radius * aspectRatio * Math.sin(Math.PI * 120 / 180))));
                    nodeArr[i + 2] = new Point((int)(size.x * (0.5 + radius * Math.cos(Math.PI * 240 / 180))), (int)(size.y * (0.5 + radius * aspectRatio * Math.sin(Math.PI * 240 / 180))));
                    i += 3;
                } break;
                case 4: {
                    float dx = random.nextFloat() * 0.8f - 0.4f;
                    float dy = random.nextFloat() * 0.8f - 0.4f;
                    nodeArr[i] = new Point((int)(size.x * (0.5 + dx)), (int)(size.y * (0.5 + dy)));
                    nodeArr[i + 1] = new Point((int)(size.x * (0.5 - dx)), (int)(size.y * (0.5 + dy)));
                    nodeArr[i + 2] = new Point((int)(size.x * (0.5 + dx)), (int)(size.y * (0.5 - dy)));
                    nodeArr[i + 3] = new Point((int)(size.x * (0.5 - dx)), (int)(size.y * (0.5 - dy)));
                    i += 4;
                } break;
                case 6: {
                    float radius = random.nextFloat() * 0.4f;
                    nodeArr[i    ] = new Point((int)(size.x * (0.5 + radius * Math.cos(Math.PI * 0 / 180))), (int)(size.y * (0.5 + radius * aspectRatio * Math.sin(Math.PI * 0 / 180))));
                    nodeArr[i + 1] = new Point((int)(size.x * (0.5 + radius * Math.cos(Math.PI * 60 / 180))), (int)(size.y * (0.5 + radius * aspectRatio * Math.sin(Math.PI * 60 / 180))));
                    nodeArr[i + 2] = new Point((int)(size.x * (0.5 + radius * Math.cos(Math.PI * 120 / 180))), (int)(size.y * (0.5 + radius * aspectRatio * Math.sin(Math.PI * 120 / 180))));
                    nodeArr[i + 3] = new Point((int)(size.x * (0.5 + radius * Math.cos(Math.PI * 180 / 180))), (int)(size.y * (0.5 + radius * aspectRatio * Math.sin(Math.PI * 180 / 180))));
                    nodeArr[i + 4] = new Point((int)(size.x * (0.5 + radius * Math.cos(Math.PI * 240 / 180))), (int)(size.y * (0.5 + radius * aspectRatio * Math.sin(Math.PI * 240 / 180))));
                    nodeArr[i + 5] = new Point((int)(size.x * (0.5 + radius * Math.cos(Math.PI * 300 / 180))), (int)(size.y * (0.5 + radius * aspectRatio * Math.sin(Math.PI * 300 / 180))));
                    i += 6;
                } break;
            }
        }
        return nodeArr;
    }
    private static Point[] getCircleCoords(int count) {
        double aspectRatio = 480.0 / 800;
        Random random = new Random();
        Point[] nodeArr = new Point[count];
        double radius = (0.2 + random.nextFloat() * 0.2);
        int shift = random.nextInt() % 100;
        Point size = new Point();
        MainMenuActivity.display.getSize(size);
        for (int i = 0; i < nodeArr.length; i++) {
            nodeArr[i] = new Point((int) (size.x * (0.5 + Math.cos(i + shift) * radius)), (int) (size.y * (0.5 + aspectRatio * Math.sin(i + shift) * radius)));
        }
        return nodeArr;
    }

    private static Point[] getSpiralCoords(int count) {
        double aspectRatio = 480.0 / 800;
        Random random = new Random();
        double radius = (0.2 + random.nextFloat() * 0.2);
        Point[] nodeArr = new Point[count];
        int shift = random.nextInt() % 100;
        Point size = new Point();
        MainMenuActivity.display.getSize(size);
        for (int i = 0; i < nodeArr.length; i++) {
            nodeArr[i] = new Point((int) (size.x * (0.5 + Math.cos((double) (i * 10) / count + shift) * radius * (double) i / count)), (int) (size.y * (0.5 + aspectRatio * Math.sin((double) (i * 10) / count + shift) * radius * (double) i / count)));
        }
        return nodeArr;
    }
    public boolean hasSolution() {
        return nodesSolution != null;
    }
    public void transpose() {
        for (int i = 0; i < nodes.length; i++) {
            float temp = nodes[i].getY();
            nodes[i].setY(nodes[i].getX());
            nodes[i].setX(temp);
        }
    }
}

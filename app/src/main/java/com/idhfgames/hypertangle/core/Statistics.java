package com.idhfgames.hypertangle.core;

import android.content.Context;
import android.content.SharedPreferences;

import com.idhfgames.hypertangle.core.observable.ObservableArrayStatistic;
import com.idhfgames.hypertangle.core.observable.ObservableStatistic;

/**
 * Created by sarik_000 on 28.07.2015.
 */
public class Statistics {
    private static Context context;
    private static SharedPreferences preferences;

    public static ObservableStatistic<Long> totalTime;
    public static ObservableStatistic<Integer> moveCount;
    public static ObservableStatistic<Integer> randomLevelsCompleted;
    public static ObservableArrayStatistic<Long> levelTimeHighScores;
    public static ObservableArrayStatistic<Integer> levelMoveCountHighScores;
    public static ObservableStatistic<Integer> skipCount;
    public static ObservableStatistic<Integer> lastSkipGatherDay;
    public static ObservableStatistic<Integer> easyLevelsCompleted;
    public static ObservableStatistic<Integer> mediumLevelsCompleted;
    public static ObservableStatistic<Integer> hardLevelsCompleted;
    public static ObservableStatistic<Integer> infernoLevelsCompleted;

    public static ObservableStatistic<Integer> easyTowersCompleted;
    public static ObservableStatistic<Integer> mediumTowersCompleted;
    public static ObservableStatistic<Integer> hardTowersCompleted;
    public static ObservableStatistic<Integer> infernoTowersCompleted;

    public static ObservableStatistic<Long> easyTowersBestTime;
    public static ObservableStatistic<Long> mediumTowersBestTime;
    public static ObservableStatistic<Long> hardTowersBestTime;
    public static ObservableStatistic<Long> infernoTowersBestTime;

    public static void init(Context context, SharedPreferences preferences) {
        Statistics.context = context;
        Statistics.preferences = preferences;

        totalTime = new ObservableStatistic<>();
        moveCount = new ObservableStatistic<>();
        randomLevelsCompleted = new ObservableStatistic<>();
        levelTimeHighScores = new ObservableArrayStatistic<>();
        levelMoveCountHighScores = new ObservableArrayStatistic<>();
        skipCount = new ObservableStatistic<>();
        lastSkipGatherDay = new ObservableStatistic<>();
        easyLevelsCompleted = new ObservableStatistic<>();
        mediumLevelsCompleted = new ObservableStatistic<>();
        hardLevelsCompleted = new ObservableStatistic<>();
        infernoLevelsCompleted = new ObservableStatistic<>();
        easyTowersCompleted = new ObservableStatistic<>();
        mediumTowersCompleted = new ObservableStatistic<>();
        hardTowersCompleted = new ObservableStatistic<>();
        infernoTowersCompleted = new ObservableStatistic<>();

        easyTowersBestTime = new ObservableStatistic<>();
        mediumTowersBestTime = new ObservableStatistic<>();
        hardTowersBestTime = new ObservableStatistic<>();
        infernoTowersBestTime = new ObservableStatistic<>();


    }
    public static void loadStatistics() {
        levelTimeHighScores.setData(new Long[Assets.getTotalLevelNum() + 1]);
        levelMoveCountHighScores.setData(new Integer[Assets.getTotalLevelNum() + 1]);
        totalTime.setData(preferences.getLong("totalTime", 0));
        moveCount.setData(preferences.getInt("moveCount", 0));
        randomLevelsCompleted.setData(preferences.getInt("randomLevelsCompleted", 0));
        for (int i = 0; i < levelTimeHighScores.getData().length; i++) {
            levelTimeHighScores.getData()[i] = preferences.getLong("level" + i + "time", -1);
        }
        for (int i = 0; i < levelMoveCountHighScores.getData().length; i++) {
            levelMoveCountHighScores.getData()[i] = preferences.getInt("level" + i + "moves", -1);
        }
        skipCount.setData(preferences.getInt("skipCount", 5));
        lastSkipGatherDay.setData(preferences.getInt("lastSkipGatherDay", 0));

        int counter = 0;
        for (int i = 1; i <= Assets.easyLevelNum; i++) {
            if (levelTimeHighScores.getData(i) != -1) {
                counter++;
            }
        }
        easyLevelsCompleted.setData(preferences.getInt("easyLevelsCompleted", counter));
        counter = 0;
        for (int i = Assets.easyLevelNum + 1; i <=  Assets.easyLevelNum + Assets.mediumLevelNum; i++) {
            if (levelTimeHighScores.getData(i) != -1) {
                counter++;
            }
        }
        mediumLevelsCompleted.setData(preferences.getInt("mediumLevelsCompleted", counter));
        counter = 0;
        for (int i = Assets.easyLevelNum + Assets.mediumLevelNum + 1; i <= Assets.easyLevelNum + Assets.mediumLevelNum + Assets.hardLevelNum; i++) {
            if (levelTimeHighScores.getData(i) != -1) {
                counter++;
            }
        }
        hardLevelsCompleted.setData(preferences.getInt("hardLevelsCompleted", counter));
        counter = 0;
        for (int i = Assets.easyLevelNum + Assets.mediumLevelNum + Assets.hardLevelNum + 1; i <= Assets.easyLevelNum + Assets.mediumLevelNum + Assets.hardLevelNum + Assets.infernoLevelNum; i++) {
            if (levelTimeHighScores.getData(i) != -1) {
                counter++;
            }
        }
        infernoLevelsCompleted.setData(preferences.getInt("infernoLevelsCompleted", counter));

        easyTowersCompleted.setData(preferences.getInt("easyTowersCompleted", 0));
        mediumTowersCompleted.setData(preferences.getInt("mediumTowersCompleted", 0));
        hardTowersCompleted.setData(preferences.getInt("hardTowersCompleted", 0));
        infernoTowersCompleted.setData(preferences.getInt("infernoTowersCompleted", 0));

        easyTowersBestTime.setData(preferences.getLong("easyTowersBestTime", -1));
        mediumTowersBestTime.setData(preferences.getLong("mediumTowersBestTime", -1));
        hardTowersBestTime.setData(preferences.getLong("hardTowersBestTime", -1));
        infernoTowersBestTime.setData(preferences.getLong("infernoTowersBestTime", -1));
    }
    public static void saveStatistics() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("totalTime", totalTime.getData());
        editor.putInt("moveCount", moveCount.getData());
        editor.putInt("randomLevelsCompleted", randomLevelsCompleted.getData());
        for (int i = 0; i < levelTimeHighScores.getData().length; i++) {
            editor.putLong("level" + i + "time", levelTimeHighScores.getData(i));
        }
        for (int i = 0; i < levelMoveCountHighScores.getData().length; i++) {
            editor.putInt("level" + i + "moves", levelMoveCountHighScores.getData(i));
        }
        editor.putInt("skipCount", skipCount.getData());
        editor.putInt("lastSkipGatherDay", lastSkipGatherDay.getData());

        editor.putInt("easyLevelsCompleted", easyLevelsCompleted.getData());
        editor.putInt("mediumLevelsCompleted", mediumLevelsCompleted.getData());
        editor.putInt("hardLevelsCompleted", hardLevelsCompleted.getData());
        editor.putInt("infernoLevelsCompleted", infernoLevelsCompleted.getData());

        editor.putInt("easyTowersCompleted", easyTowersCompleted.getData());
        editor.putInt("mediumTowersCompleted", mediumTowersCompleted.getData());
        editor.putInt("hardTowersCompleted", hardTowersCompleted.getData());
        editor.putInt("infernoTowersCompleted", infernoTowersCompleted.getData());

        editor.putLong("easyTowersBestTime", easyTowersBestTime.getData());
        editor.putLong("mediumTowersBestTime", mediumTowersBestTime.getData());
        editor.putLong("hardTowersBestTime", hardTowersBestTime.getData());
        editor.putLong("infernoTowersBestTime", infernoTowersBestTime.getData());

        editor.commit();
    }
    public static void resetStatistics() {
        randomLevelsCompleted.setData(0);
        for (int i = 0; i < levelTimeHighScores.getData().length; i++) {
            levelTimeHighScores.setData(-1L, i);
        }
        for (int i = 0; i < levelMoveCountHighScores.getData().length; i++) {
            levelMoveCountHighScores.setData(-1, i);
        }


        easyLevelsCompleted.setData(0);
        mediumLevelsCompleted.setData(0);
        hardLevelsCompleted.setData(0);
        infernoLevelsCompleted.setData(0);

        easyTowersCompleted.setData(0);
        mediumTowersCompleted.setData(0);
        hardTowersCompleted.setData(0);
        infernoTowersCompleted.setData(0);

        easyTowersBestTime.setData(-1L);
        mediumTowersBestTime.setData(-1L);
        hardTowersBestTime.setData(-1L);
        infernoTowersBestTime.setData(-1L);

    }
    public static void notifyAchievements() {
        easyLevelsCompleted.notifyObservers();
        mediumLevelsCompleted.notifyObservers();
        hardLevelsCompleted.notifyObservers();
        infernoLevelsCompleted.notifyObservers();

        easyTowersCompleted.notifyObservers();
        mediumTowersCompleted.notifyObservers();
        hardTowersCompleted.notifyObservers();
        infernoTowersCompleted.notifyObservers();

        totalTime.notifyObservers();
    }
}

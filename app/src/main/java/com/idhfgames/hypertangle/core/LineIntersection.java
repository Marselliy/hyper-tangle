package com.idhfgames.hypertangle.core;

import android.graphics.Point;

/**
 * Created by sarik_000 on 29.07.2015.
 */
public class LineIntersection {
    private static int orientation(Point p0, Point p1, Point p2)
    {
        int x1= p1.x- p0.x;
        int y1 = p1.y - p0.y;
        int x2= p2.x- p0.x;
        int y2 = p2.y - p0.y;

        int determinant = x1*y2 - x2*y1;

        if(determinant==0)
            return 0;

        if(determinant>0)
            return 1;
        return 2;
    }
    private static int onsegment(Point p0, Point p1, Point p2)
    {
        if(p2.x >= Math.min(p0.x, p1.x)&& p2.x <= Math.max(p0.x, p1.x))
            return 1;

        return 0;
    }
    public static boolean check(Point p1, Point q1, Point p2, Point q2)
    {
        if (p1.x == p2.x && q1.x == q2.x && p1.y == p2.y && q1.y == q2.y)
            return false;
        if (p1.equals(p2.x, p2.y) || p1.equals(q2.x, q2.y) || q1.equals(p2.x, p2.y) || q1.equals(q2.x, q2.y))
            return false;

        int o1 = orientation(p1,q1,q2);
        int o2 = orientation(p1,q1,p2);
        int o3 = orientation(p2,q2,p1);
        int o4 = orientation(p2,q2,q1);

        if(o1!=o2&&o3!=o4)  //handles general cases
            return true;

        if(o1==0&&o2==0&&o3==0&&o4==0)  //handles special cases when all four points are collinear
        {
            if(onsegment(p1,q1,p2) != 0||onsegment(p1,q1,q2) != 0)
                return true;
        }
        return false;

    }
}

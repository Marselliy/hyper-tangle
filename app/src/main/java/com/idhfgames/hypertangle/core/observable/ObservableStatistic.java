package com.idhfgames.hypertangle.core.observable;

import java.util.Observable;

/**
 * Created by sarik_000 on 01.08.2015.
 */
public class ObservableStatistic<T> extends Observable{
    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
        setChanged();
        notifyObservers();
    }
}

package com.idhfgames.hypertangle.core;

/**
 * Created by sarik_000 on 13-Aug-15.
 */
public class AdTicker {
    private static int adFlag = 0;
    private static int adFreq = 4;
    public static boolean isTime() {
        adFlag = (adFlag + 1) % adFreq;
        return adFlag == 0;
    }
}

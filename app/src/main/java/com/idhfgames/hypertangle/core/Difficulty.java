package com.idhfgames.hypertangle.core;

/**
 * Created by sarik_000 on 04-Aug-15.
 */
public enum Difficulty {
    EASY,
    MEDIUM,
    HARD,
    INFERNO
}
package com.idhfgames.hypertangle.core.observable;

import java.util.Observable;

/**
 * Created by sarik_000 on 01.08.2015.
 */
public class ObservableArrayStatistic<T> extends Observable {
    private T[] data;

    public T[] getData() {
        return data;
    }
    public T getData(int index) {
        return data[index];
    }

    public void setData(T data, int index) {
        this.data[index] = data;
        setChanged();
        notifyObservers();
    }
    public void setData(T[] data) {
        this.data = data;
        setChanged();
        notifyObservers();
    }
}

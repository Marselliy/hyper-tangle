package com.idhfgames.hypertangle.core;

import android.util.Log;

import com.google.android.gms.games.Games;
import com.idhfgames.hypertangle.MainMenuActivity;
import com.idhfgames.hypertangle.core.observable.ObservableArrayStatistic;
import com.idhfgames.hypertangle.core.observable.ObservableStatistic;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by sarik_000 on 01.08.2015.
 */
public class Achievment implements Observer{
    private int needValue;
    private int index;
    private String id;

    public Achievment(int needValue, int index, String id) {
        this.needValue = needValue;
        this.index = index;
        this.id = id;
    }

    @Override
    public void update(Observable observable, Object data) {
        if (MainMenuActivity.googleApiClient != null && MainMenuActivity.googleApiClient.isConnected()) {
            if (observable instanceof ObservableStatistic) {
                if (needValue <= Integer.parseInt(((ObservableStatistic) observable).getData().toString())) {
                    Games.Achievements.unlock(MainMenuActivity.googleApiClient, id);
                }
            }
        }
    }
}

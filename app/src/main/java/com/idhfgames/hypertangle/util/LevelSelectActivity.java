package com.idhfgames.hypertangle.util;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.idhfgames.hypertangle.GameActivity;
import com.idhfgames.hypertangle.MainMenuActivity;
import com.idhfgames.hypertangle.R;
import com.idhfgames.hypertangle.core.Assets;
import com.idhfgames.hypertangle.core.Graph;
import com.idhfgames.hypertangle.core.Statistics;
import com.idhfgames.hypertangle.util.util.SystemUiHider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class LevelSelectActivity extends Activity {

    FrameLayout level_select_mainFrame;
    FrameLayout dimBackground;
    TextView levelSelectView;
    TextView levelSelectViewEasy;
    TextView levelSelectViewMedium;
    TextView levelSelectViewHard;
    TextView levelSelectViewInferno;
    TableLayout levelSelectTableEasy;
    TableLayout levelSelectTableMedium;
    TableLayout levelSelectTableHard;
    TableLayout levelSelectTableInferno;
    TableRow[] levelSelectTableRowsEasy;
    TableRow[] levelSelectTableRowsMedium;
    TableRow[] levelSelectTableRowsHard;
    TableRow[] levelSelectTableRowsInferno;
    public static Button[] levelButtonsEasy;
    public static Button[] levelButtonsMedium;
    public static Button[] levelButtonsHard;
    public static Button[] levelButtonsInferno;
    Button levelSelectButtonRandom;
    Button buttonPlus;
    Button buttonMinus;
    TextView textViewNodeCount;
    private int lastLevelClickedId;
    private PopupWindow levelInfo;
    private Context context = this;
    public static int randomNodeCount = 10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            ActionBar actionBar = getActionBar();
            actionBar.hide();
        }
        setContentView(R.layout.activity_level_select);
        level_select_mainFrame = (FrameLayout) findViewById(R.id.level_select_mainFrame);
        dimBackground = (FrameLayout) findViewById(R.id.dimBackground);
        levelSelectView = (TextView) findViewById(R.id.textView);
        levelSelectView.setTypeface(MainMenuActivity.addTypeface);
        levelSelectViewEasy = (TextView) findViewById(R.id.textViewEasy);
        levelSelectViewEasy.setTypeface(MainMenuActivity.mainTypeface);
        levelSelectViewEasy.setText(getResources().getString(R.string.easy) + " " + Statistics.easyLevelsCompleted.getData() + "/" + Assets.easyLevelNum);
        levelSelectViewMedium = (TextView) findViewById(R.id.textViewMedium);
        levelSelectViewMedium.setTypeface(MainMenuActivity.mainTypeface);
        levelSelectViewMedium.setText(getResources().getString(R.string.medium) + " " + Statistics.mediumLevelsCompleted.getData() + "/" + Assets.mediumLevelNum);
        levelSelectViewHard = (TextView) findViewById(R.id.textViewHard);
        levelSelectViewHard.setTypeface(MainMenuActivity.mainTypeface);
        levelSelectViewHard.setText(getResources().getString(R.string.hard) + " " + Statistics.hardLevelsCompleted.getData() + "/" + Assets.hardLevelNum);
        levelSelectViewInferno = (TextView) findViewById(R.id.textViewInferno);
        levelSelectViewInferno.setTypeface(MainMenuActivity.mainTypeface);
        levelSelectViewInferno.setText(getResources().getString(R.string.inferno) + " " + Statistics.infernoLevelsCompleted.getData() + "/" + Assets.infernoLevelNum);
        levelSelectButtonRandom = (Button)findViewById(R.id.levelSelectButtonRandom);
        levelSelectButtonRandom.setTypeface(MainMenuActivity.mainTypeface);
        levelSelectTableEasy = (TableLayout)findViewById(R.id.levelSelectTableEasy);
        levelSelectTableRowsEasy = new TableRow[levelSelectTableEasy.getChildCount()];
        levelSelectTableMedium = (TableLayout) findViewById(R.id.levelSelectTableMedium);
        levelSelectTableRowsMedium = new TableRow[levelSelectTableMedium.getChildCount()];
        levelSelectTableHard = (TableLayout)findViewById(R.id.levelSelectTableHard);
        levelSelectTableRowsHard = new TableRow[levelSelectTableHard.getChildCount()];
        levelSelectTableInferno = (TableLayout) findViewById(R.id.levelSelectTableInferno);
        levelSelectTableRowsInferno = new TableRow[levelSelectTableInferno.getChildCount()];
        for (int i = 0; i < levelSelectTableRowsEasy.length; i++) {
            levelSelectTableRowsEasy[i] = (TableRow) levelSelectTableEasy.getChildAt(i);
        }
        for (int i = 0; i < levelSelectTableRowsMedium.length; i++) {
            levelSelectTableRowsMedium[i] = (TableRow) levelSelectTableMedium.getChildAt(i);
        }
        for (int i = 0; i < levelSelectTableRowsHard.length; i++) {
            levelSelectTableRowsHard[i] = (TableRow) levelSelectTableHard.getChildAt(i);
        }
        for (int i = 0; i < levelSelectTableRowsInferno.length; i++) {
            levelSelectTableRowsInferno[i] = (TableRow) levelSelectTableInferno.getChildAt(i);
        }
        levelButtonsEasy = new Button[Assets.easyLevelNum];
        levelButtonsMedium = new Button[Assets.mediumLevelNum];
        levelButtonsHard = new Button[Assets.hardLevelNum];
        levelButtonsInferno = new Button[Assets.infernoLevelNum];

        textViewNodeCount = (TextView)findViewById(R.id.textViewNodeCount);
        textViewNodeCount.setText("" + randomNodeCount);
        textViewNodeCount.setTypeface(MainMenuActivity.mainTypeface);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        Button temp = null;
        TableRow.LayoutParams params = new TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(3, 3, 3, 3);
        int counter = 0;
        for (int i = 0; i < Assets.easyLevelNum; i++) {
            if (Statistics.levelTimeHighScores.getData(i + 1) == -1) {
                counter++;
            }
            temp = initializeLevelButton(size, i + 1, "" + (i + 1), counter > 5);
            temp.setLayoutParams(params);
            levelSelectTableRowsEasy[i % levelSelectTableRowsEasy.length].addView(temp);
        }
        counter = 0;
        for (int i = 0; i < Assets.mediumLevelNum; i++) {
            if (Statistics.levelTimeHighScores.getData(i + 1 + Assets.easyLevelNum) == -1) {
                counter++;
            }
            temp = initializeLevelButton(size, i + 1 + Assets.easyLevelNum, "" + (i + 1), counter > 5);
            temp.setLayoutParams(params);
            levelSelectTableRowsMedium[i % levelSelectTableRowsMedium.length].addView(temp);
        }
        counter = 0;
        for (int i = 0; i < Assets.hardLevelNum; i++) {
            if (Statistics.levelTimeHighScores.getData(i + 1 + Assets.easyLevelNum + Assets.mediumLevelNum) == -1) {
                counter++;
            }
            temp = initializeLevelButton(size, i + 1 + Assets.easyLevelNum + Assets.mediumLevelNum, "" + (i + 1), counter > 5);
            temp.setLayoutParams(params);
            levelSelectTableRowsHard[i % levelSelectTableRowsHard.length].addView(temp);
        }
        counter = 0;
        for (int i = 0; i < Assets.infernoLevelNum; i++) {
            if (Statistics.levelTimeHighScores.getData(i + 1 + Assets.easyLevelNum + Assets.mediumLevelNum + Assets.hardLevelNum) == -1) {
                counter++;
            }
            temp = initializeLevelButton(size, i + 1 + Assets.easyLevelNum + Assets.mediumLevelNum + Assets.hardLevelNum, "" + (i + 1), counter > 5);
            temp.setLayoutParams(params);
            levelSelectTableRowsInferno[i % levelSelectTableRowsInferno.length].addView(temp);
        }

        levelInfo = new PopupWindow(this);



    }
    public void onButtonRandomClick(View view) {
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        Graph graph = Graph.getRandomGraph(4, randomNodeCount, Graph.TYPE.NONSCATTERED);
        graph.isPlanar();

        GameActivity.setGraph(graph);
        GameActivity.setCurLevelId(0);
        this.finish();
    }
    public void loadLevel(int id) {
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        StringBuilder data = new StringBuilder("");
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(getAssets().open("levels/" + id + ".lvl")));

            String mLine = reader.readLine();
            while (mLine != null) {
                data = data.append(mLine);

                mLine = reader.readLine();
            }
        } catch (IOException e) {
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
        }
        Graph graph = new Graph(data.toString());
        graph.isPlanar();
        GameActivity.setGraph(new Graph(data.toString()));
        GameActivity.setCurLevelId(id);
        this.finish();
    }
    private Button initializeLevelButton(Point size, int id, String text, boolean locked) {
        Button button = new Button(this);

        if (locked) {
            button = (Button) getLayoutInflater().inflate(R.layout.template_level_button_locked, null);
        } else {
            if (Statistics.levelTimeHighScores.getData(id) != -1) {
                if (id <= Assets.easyLevelNum) {
                    button = (Button) getLayoutInflater().inflate(R.layout.template_level_select_button_green_competed, null);
                } else if (id <= Assets.easyLevelNum + Assets.mediumLevelNum) {
                    button = (Button) getLayoutInflater().inflate(R.layout.template_level_select_button_orange_competed, null);
                } else if (id <= Assets.easyLevelNum + Assets.mediumLevelNum + Assets.hardLevelNum) {
                    button = (Button) getLayoutInflater().inflate(R.layout.template_level_select_button_red_competed, null);
                } else {
                    button = (Button) getLayoutInflater().inflate(R.layout.template_level_select_button_violet_competed, null);
                }
            } else {
                if (id <= Assets.easyLevelNum) {
                    button = (Button) getLayoutInflater().inflate(R.layout.template_level_select_button_green, null);
                } else if (id <= Assets.easyLevelNum + Assets.mediumLevelNum) {
                    button = (Button) getLayoutInflater().inflate(R.layout.template_level_select_button_orange, null);
                } else if (id <= Assets.easyLevelNum + Assets.mediumLevelNum + Assets.hardLevelNum) {
                    button = (Button) getLayoutInflater().inflate(R.layout.template_level_select_button_red, null);
                } else {
                    button = (Button) getLayoutInflater().inflate(R.layout.template_level_select_button_violet, null);
                }
            }
        }
        button.setTextSize(50);
        if (id >= Assets.easyLevelNum + Assets.mediumLevelNum + Assets.hardLevelNum + 100)
            button.setTextSize(36);
        button.setWidth(size.x / 5);
        button.setHeight(size.x / 5);
        button.setText(text);
        button.setTypeface(MainMenuActivity.mainTypeface);
        button.setId(id);
        if (!locked) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastLevelClickedId = v.getId();

                    LinearLayout llMain = new LinearLayout(context);
                    llMain.setOrientation(LinearLayout.VERTICAL);
                    llMain.setBackgroundColor(getResources().getColor(R.color.background_color));
                    llMain.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    llMain.setBackgroundDrawable(getResources().getDrawable(R.drawable.popup_level_finish));
                    llMain.setPadding(5, 5, 5, 5);

                    final TextView textViewPopupHeader = new TextView(context);
                    textViewPopupHeader.setTypeface(MainMenuActivity.addTypeface);
                    textViewPopupHeader.setText("Level " + lastLevelClickedId + " information");
                    textViewPopupHeader.setTextSize(34);
                    textViewPopupHeader.setTextColor(getResources().getColor(R.color.flat_white));
                    textViewPopupHeader.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    llMain.addView(textViewPopupHeader);


                    TextView textViewState = new TextView(context);
                    textViewState.setText(Statistics.levelMoveCountHighScores.getData(lastLevelClickedId) != -1 ? (Statistics.levelMoveCountHighScores.getData(lastLevelClickedId) != 1000000 ? "(completed)" : "(skipped)") : "(incompleted)");
                    textViewState.setTextSize(18);
                    textViewState.setTextColor(getResources().getColor(R.color.flat_white));
                    textViewState.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    llMain.addView(textViewState);


                    int time = (int)(long)(Statistics.levelTimeHighScores.getData(lastLevelClickedId));
                    TextView textViewTime = new TextView(context);
                    if (time != -1 && time != 1000000L) {
                        int hours = time / 3600000;
                        int minutes = (time - hours * 3600000) / 60000;
                        int seconds = (time - hours * 3600000 - minutes * 60000) / 1000;
                        int ms = time - hours * 3600000 - minutes * 60000 - seconds * 1000;
                        textViewTime.setText(getResources().getString(R.string.best_time) + " : " + (hours == 0 ? "" : (hours + "h ")) + (minutes == 0 ? "" : (minutes + "m ")) + seconds + "," + (ms < 10 ? "00" + ms : (ms < 100 ? "0" + ms : ms)) + "s");
                    } else {
                        textViewTime.setText(getResources().getString(R.string.best_time) + " : ");
                    }
                    textViewTime.setTextSize(21);
                    textViewTime.setTextColor(getResources().getColor(R.color.flat_white));
                    textViewTime.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    llMain.addView(textViewTime);

                    TextView textViewMoves = new TextView(context);

                    textViewMoves.setText("Best move count : " + ((Statistics.levelMoveCountHighScores.getData(lastLevelClickedId) != -1 && Statistics.levelMoveCountHighScores.getData(lastLevelClickedId) != 1000000) ? Statistics.levelMoveCountHighScores.getData(lastLevelClickedId) : ""));

                    textViewMoves.setTextSize(21);
                    textViewMoves.setTextColor(getResources().getColor(R.color.flat_white));
                    textViewMoves.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    llMain.addView(textViewMoves);

                    LinearLayout rlAdd = new LinearLayout(context);
                    rlAdd.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
                    params.setMargins(3, 3, 3, 3);

                    Button buttonBack = new Button(context);
                    buttonBack.setTypeface(MainMenuActivity.addTypeface);
                    buttonBack = (Button)getLayoutInflater().inflate(getResources().getLayout(R.layout.template_level_select_button_red), null);
                    buttonBack.setLayoutParams(params);

            //        buttonBack.setTextColor(getResources().getColor(R.color.main_menu_font_color));
            //        buttonBack.setBackgroundColor(getResources().getColor(R.color.background_color));
                    buttonBack.setTextSize(26);
                    buttonBack.setText(getResources().getString(R.string.back));
                    buttonBack.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            MainMenuActivity.mpClick.start();
                            levelInfo.dismiss();
                        //    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            dimBackground.setVisibility(View.GONE);
                        }
                    });
                    rlAdd.addView(buttonBack);

                    Button buttonPlay = new Button(context);
                    buttonPlay.setTypeface(MainMenuActivity.addTypeface);
                    buttonPlay = (Button)getLayoutInflater().inflate(getResources().getLayout(R.layout.template_level_select_button_green), null);
                    buttonPlay.setLayoutParams(params);
                    buttonPlay.setTextSize(26);
                    buttonPlay.setText(getResources().getString(R.string.start));
                    buttonPlay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            MainMenuActivity.mpClick.start();
                            levelInfo.dismiss();
                            loadLevel(lastLevelClickedId);
                            dimBackground.setVisibility(View.GONE);
                        }
                    });
                    rlAdd.addView(buttonPlay);

                    llMain.addView(rlAdd);

                    dimBackground.setVisibility(View.VISIBLE);

                    levelInfo.setContentView(llMain);

                    levelInfo.showAtLocation(level_select_mainFrame, Gravity.CENTER, 0, 0);
                    levelInfo.setWindowLayoutMode(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    levelInfo.update(0, 0, 300, 80);

                }
            });
        }
        return button;
    }
    public void onMinusClick(View view) {
        if (--randomNodeCount < 10) {
            randomNodeCount = 10;
        }
        textViewNodeCount.setText("" + randomNodeCount);
    }
    public void onPlusClick(View view) {
        if (++randomNodeCount > 99) {
            randomNodeCount = 99;
        }
        textViewNodeCount.setText("" + randomNodeCount);
    }
    @Override
    public void onBackPressed() {
        this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}

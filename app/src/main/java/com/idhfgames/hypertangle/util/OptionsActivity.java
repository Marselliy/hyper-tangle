package com.idhfgames.hypertangle.util;


import android.app.ActionBar;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;

import com.idhfgames.hypertangle.R;
import com.idhfgames.hypertangle.core.Options;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class OptionsActivity extends Activity {
    CheckBox checkBoxRealTime;
    private StartAppAd startAppAd = new StartAppAd(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            ActionBar actionBar = getActionBar();
            actionBar.hide();
        }
        StartAppSDK.init(this, getString(R.string.startapp_id), true);
        setContentView(R.layout.activity_options);
        checkBoxRealTime = (CheckBox) findViewById(R.id.checkBoxRealTime);
        checkBoxRealTime.setChecked(Options.realTimeIntersections);
        startAppAd.showAd();
        startAppAd.loadAd();
    }
    public void onBackButtonClick(View view) {
        this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    public void onSaveButtonClick(View view) {
        Options.realTimeIntersections = checkBoxRealTime.isChecked();
        this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    @Override
    public void onBackPressed() {
        this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    @Override
    public void onResume() {
        super.onResume();
        startAppAd.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        startAppAd.onPause();
    }
}

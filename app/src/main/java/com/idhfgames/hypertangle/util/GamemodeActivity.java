package com.idhfgames.hypertangle.util;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.idhfgames.hypertangle.MainMenuActivity;
import com.idhfgames.hypertangle.R;
import com.idhfgames.hypertangle.TowerActivity;
import com.idhfgames.hypertangle.TowerContinueActivity;
import com.idhfgames.hypertangle.TowerDifficultyActivity;
import com.idhfgames.hypertangle.TutorialActivity;
import com.idhfgames.hypertangle.util.util.SystemUiHider;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;


public class GamemodeActivity extends Activity {
    public static boolean isActive = false;
    Button buttonLevelSelect;
    Button buttonTower;
    Button buttonTutorial;
    private StartAppAd startAppAd = new StartAppAd(this);
    @Override
    public void onStart() {
        super.onStart();
        isActive = true;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        isActive = false;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            ActionBar actionBar = getActionBar();
            actionBar.hide();
        }
        StartAppSDK.init(this, getString(R.string.startapp_id), true);
        setContentView(R.layout.activity_gamemode);

        buttonLevelSelect = (Button) findViewById(R.id.buttonLevelSelect);
        buttonTower = (Button) findViewById(R.id.buttonTower);
        buttonTutorial = (Button) findViewById(R.id.buttonTutorial);

        buttonLevelSelect.setTypeface(MainMenuActivity.mainTypeface);
        buttonTower.setTypeface(MainMenuActivity.mainTypeface);
        buttonTutorial.setTypeface(MainMenuActivity.mainTypeface);
        startAppAd.showAd();
        startAppAd.loadAd();
    }
    public void onLevelSelectButtonClick(View view) {
        MainMenuActivity.mpClick.start();
        Intent intent = new Intent(this, LevelSelectActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    public void onTowerButtonClick(View view) {
        MainMenuActivity.mpClick.start();
        Intent intent;
        if (TowerActivity.graphs == null)
            intent = new Intent(this, TowerDifficultyActivity.class);
        else
            intent = new Intent(this, TowerContinueActivity.class);
        this.finish();
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    public void onTutorialButtonClick(View view) {
        MainMenuActivity.mpClick.start();
        Intent intent = new Intent(this, TutorialActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    @Override
    public void onBackPressed() {
        this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    @Override
    public void onResume() {
        super.onResume();
        startAppAd.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        startAppAd.onPause();
    }
}
